# FabLab-NIX

## SOPS Secrets

Secrets are stored encrypted in `/secrets` using [sops](https://github.com/getsops/sops).

To add new hosts for secrets, add them to `/.sops.yaml` and re-encrypt the affected files.

## Troubleshooting

If you get this error

```text
'/nix/store/6z1pvw6whcc1a4mpr7y6v5qdh50qcs1n-system-path/bin/busctl --json=short call org.freedesktop.systemd1 /org/freedesktop/systemd1 org.freedesktop.systemd1.Manager ListUnitsByPatterns asas 0 0' exited with value 1 at /nix/store/q15zj8fb4aq0h248i0260v8kh0g6cgy6-nixos-system-c2-22.11.3355.44302d48a0b/bin/switch-to-configuration line 140.
Error while activating new configuration.
```

SSH in and restart `dbus.service`

## TODOs

### Kubernetes

- Add monitoring (e.g. via prometheus) to the hosts (via kubernetes?)
- Create a grafana alert for old bios version

### Server hardware

- 3D-model and print adapters for 2.5" drive caddies 

### Distribute storage

- Investigate seaweedfs, glusterfs and moosefs as ceph alternatives
