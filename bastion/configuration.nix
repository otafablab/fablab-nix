{ config, pkgs, ... }:
{
  imports = [
    ./hardware-configuration.nix
    ../modules/admins.nix
    ../modules/autoUpdate.nix
    ../modules/core.nix
    ./services
    # <nixpkgs/nixos/modules/profiles/hardened.nix> # breaks ssh?
  ];

  security.acme = {
    acceptTerms = true;
    defaults.email = "niklas.halonen@protonmail.com";
    defaults.group = "nginx";
    certs."fablab-systems.fi" = {
      # Comment out for production server after testing with staging
      # server = "https://acme-staging-v02.api.letsencrypt.org/directory";
      dnsProvider = "cloudflare";
      extraDomainNames = [ "*.fablab-systems.fi" ];
      environmentFile = config.sops.secrets."cloudflare/lego-env".path;
    };
  };

  # Use the GRUB 2 boot loader.
  boot.loader.grub = {
    enable = true;
    device = "/dev/sda"; # or "nodev" for efi only
  };

  networking = {
    wireguard.interfaces.ext = {
      ips = [ "10.126.0.1/24" ];
      listenPort = 51820;
      privateKeyFile = config.sops.secrets."wg/privatekey".path;
      peers = [
        {
          publicKey = "LbvAP4uRvVsoKEhhoyt2s7eTC4RrYD4svv3hHyQ39jc=";
          allowedIPs = [
            "10.126.0.2/32" # rtr
            "10.42.0.0/24" # Fablab internal network
          ];
        }
      ];
    };
    wireguard.interfaces.otawilma = {
      ips = [ "10.128.0.1/24" ];
      listenPort = 51840;
      privateKeyFile = config.sops.secrets."otawilma/wg/privatekey".path;
      peers = [
        {
          publicKey = "W+b3buap4s4gbVchO8FYeE+9qymcwpHmHenN66BE5x0=";
          allowedIPs = [
            "10.128.0.2/32" # serbonk
          ];
        }
      ];
    };
    # wireguard.interfaces.datanurkka = {
    #   ips = ["10.127.0.1/24"];
    #   listenPort = 51830;
    #   privateKeyFile = config.sops.secrets."wg/privatekey".path;
    #   peers = [
    #     {
    #       publicKey = "0mRZBo1oK840bGWR7kLJwnpVvPIsVOn1/ykkAmNXYTQ=";
    #       allowedIPs = [
    #         "10.127.0.2/32"
    #         "10.91.0.0/16"
    #         "10.81.0.0/16"
    #       ];
    #     }
    #   ];
    # };
    firewall.enable = false;
    nftables = {
      enable = true;
      # https://wiki.nftables.org/wiki-nftables/index.php/Netfilter_hooks
      ruleset = ''
        define WAN_IFACE = "ens3"
        define WG_IFACE = "ext"
        define OW_IFACE = "otawilma"

        table ip filter {
          chain input {
            type filter hook input priority 0; policy drop;

            # accept all loopback packets
            iifname "lo" accept
            # iifname "podman0" udp dport 53 accept comments "Podman DNS" # Enable if podman is enabled
            
            # accept all icmp packets
            meta l4proto icmp accept
            # accept all packets that are part of an already-established connection
            ct state established,related accept

            # accept all SSH packets received on a public interface
            iifname $WAN_IFACE tcp dport ssh accept
            # accept all WireGuard packets received on a public interface
            # TODO do we need to add all prerouting ports here? probably not based on hook diagram
            iifname $WAN_IFACE udp dport 51820 accept comment "Fablab VPN"
            # iifname $WAN_IFACE udp dport 51830 accept comment "Datanurkka VPN"
            iifname $WAN_IFACE udp dport 51840 accept comment "OtaWilma VPN"

            # accept all HTTP traffic received on a public interface
            iifname $WAN_IFACE tcp dport {80,443} accept
            
            # accept db.rannasta-suomeen.fi postgresql
            iifname $WAN_IFACE tcp dport 5432 accept

            # accept ssh and web connections from ext
            iifname $WG_IFACE tcp dport {22,80,443} accept

            iifname $WAN_IFACE counter drop comment "Drop all other unsolicited traffic from wan"
          }

          chain forward {
            type filter hook forward priority 0; policy drop;
          }

          chain prerouting {
            type nat hook prerouting priority -100; policy accept;
            iifname $WAN_IFACE udp dport {53,80,123,443,1194,1197,1198,8080,9201} redirect to 51820
          }
        }

        table ip6 filter {
          chain input {
            type filter hook input priority 0; policy drop;
          }
          chain forward {
            type filter hook forward priority 0; policy drop;
          }
        }
      '';
    };
  };

  users = {
    users = {
      admin = {
        isNormalUser = true;
        extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
        openssh.authorizedKeys.keyFiles = config.users.users.root.openssh.authorizedKeys.keyFiles;
      };

      jump = {
        isSystemUser = true;
        group = "jump";
        openssh.authorizedKeys.keyFiles = config.users.users.root.openssh.authorizedKeys.keyFiles ++ [
          ../keys/users/sanduuz
        ];
        openssh.authorizedKeys.keys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMB97fmVBBRfnEkHwwIzAw2HuFw4ETirX+Rm6kAzbK+u haskell"
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJON4QLbXCALywArx38vHVulfk+6KBuSXB/j1INacb+g haskell_server"
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK5eM8J73yRbWvoieUufejFpZq2OE7/93zc51G44r7qy niklash"
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBCVfi/1+djOf76v0zFocQbVwzJUWD9QwrxuPu1no5cW"
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIO67UftwEsqcGwcfTzwD6AizxDfCHQeJtC2MwmGqzogs"
          "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDZUhi/tCxZusWmMzTkDFgmszKw39M4pFq9OeqHzzks2/7rVCDbjMU2z5iF2UeWDBS48I/WLBUXBc3mjRYYyQUXaoMch00izOtdh+DrO5MDpTHSbBO2coSXwIgzQfaoZZTimFBsbmcd/0sLiJH59z48YQVKlKgCVd0KLuQLMPdm+HqGTcoySMs2VIINVd93vMboOR5hs1MU1kuIFZKIYYO9znzOkq/lS9jtGclvRKOlBWRc1pAIifCeQ7gjB6XhbwLSOu3G8ZD8nFdByKm399hOTzMlZ1CZf034sNEBvnzgwmzKDOjrX6JtbJ17scZbti7z3EPU131XGlV6KX7Dw58eEWIs3qJ+NSuWkFPqEtXu48IeIXoiHZr/CIcbDhRhRqt32xD01BiJ/Sae0mwhTRDqsHjrNRa8aitJ2oOeXbej562oFttC8Uq3/Y6H1yO1XQ3NZ0ROtwqorHc+UbVieVWbL4C1hFqOHckbYd/+S+AKBmQQ32P0SqHd+QAE5qFPw9U= tuukk@desktop-tuukk"
          "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCvn6sgnfJsDc1vG7XIEhqd6gNzYlJdYa8qo4Q0m9GS/re/PN5J1iWWrpYXC4/2GB82pe3o2uxEi+VYLuzgZ+IfGcUYTS46ihS+ZkMkXAgI3NX590CM3+qSCXLMKQkxHgO4+R9jXnpOrNXU+otOjkIIQJg4cfp8eqwO8RoCV9lUt2aGyxDWo9tRWStBs5GGrLXVmQmGlxWcjYaCN6d0ptjx6bhDzmuIbNewxBc7wLtPrL8zs3OSy5HJ2IXW/nn7kOmw/hgikIsP81e/+RuEvhX5Q9Gd9GqWFttllQEL6IkvvDUs3ehIxfPQt5kfBkraDaAHxrNn7trwHf7YJzvpRqSv9Al/bIgHdB3OmbClGV1DJf0SW1VWWPWFfArUZT/ePnDA9hyOzHuw7euYzOzZnMBQ611ZKQ9XZBn8QUbY9bPRizupxFXRm/lSdV15HMOQLfnmvu2l5H6qJckMe7ROElz+HRTqOGF7CvU7QxVQFoS1gfTd9r2/xHEB/Q/ZiCcP+76+LmsWazN9/nYN/2QdPRhaBbtFLzk+ueztJUsyo/CCH5MgOwWEKzca5J9cHkcJWUXiumj4+8aLyWTly5ersilcHkJeb16HIAXypWYr9n0CismKMGC9iCSoC+Y4D0UQvLhQwKpt7TNX3BvRdDK4vk8b93Bp/9sbJjFslJju7FX0mw== alma-n-github"
        ];
      };
    };
    groups.jump = { };
  };

  sops = {
    defaultSopsFile = ../secrets/bastion.yaml;
    secrets."wg/privatekey" = { };
    secrets."otawilma/wg/privatekey" = { };
    secrets."cloudflare/lego-env" = {
      owner = "acme";
      group = "acme";
    };
  };

  environment.defaultPackages = with pkgs; [ python3 ];

  system.stateVersion = "22.11";
}
