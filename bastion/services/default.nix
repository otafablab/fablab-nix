{ inputs, ... }:
{
  imports = [
    ./logic.nix

    # infosec-course infrastructure
    # ./headscale.nix
    # ./infosec-authenticate.nix
  ];
  services = {
    # grafana = {
    #   enable = true;
    #   settings.server = {
    #     http_addr = "127.0.0.1";
    #     port = 3000;
    #     # domain = "bastion.fablab.rip";
    #     root_url = "http://localhost/grafana/";
    #     serve_from_sub_path = true;
    #   };
    #   provision = {
    #     enable = true;
    #     datasources.settings.datasources = [
    #       {
    #         name = "Prometheus c1";
    #         type = "prometheus";
    #         access = "proxy";
    #         url = "http://10.42.0.9:9001";
    #       }
    #       {
    #         name = "Loki c1";
    #         type = "loki";
    #         access = "proxy";
    #         url = "http://10.42.0.9:3030";
    #       }
    #     ];
    #   };
    # };

    nginx = {
      enable = true;

      # Use recommended settings
      recommendedGzipSettings = true;
      recommendedOptimisation = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;

      # Only allow PFS-enabled ciphers with AES256
      sslCiphers = "AES256+EECDH:AES256+EDH:!aNULL";

      virtualHosts."fablab-systems.fi" = {
        useACMEHost = "fablab-systems.fi";
        addSSL = true;
        # FIXME proxy_ssl_verify????
        extraConfig = ''
          client_max_body_size 1000m;
          proxy_ssl_server_name on;
          proxy_ssl_name $host;
          proxy_buffer_size 128k;
          proxy_buffers 64 512k;
          proxy_cache off;
        '';

        locations."/" = {
          root = (import inputs.wiki { system = "x86_64-linux"; }).build;
        };

        # FIXME add authentication
        # locations."/" = {
        #   proxyPass = "https://fablab-kube";
        #   proxyWebsockets = true;
        # };
      };

      virtualHosts."tt.otaniemi.org" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          root = (inputs.infosec-course.packages.x86_64-linux.website);
        };
        locations."/setup.sh" = {
          return = "301 https://gitlab.com/otafablab/infosec-course/-/raw/master/bin/setup.sh";
        };
        locations."/vpn.sh" = {
          return = "301 https://gitlab.com/otafablab/infosec-course/-/raw/master/bin/vpn.sh";
        };
        extraConfig = ''
          error_page 404 /404.html;
        '';
      };

      virtualHosts."kartat-api.otawilma.fi" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://kartat-api-otawilma";
          proxyWebsockets = true;
        };
      };

      virtualHosts."prometheus.otawilma.fi" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://prometheus-otawilma";
          proxyWebsockets = true;
        };
      };

      virtualHosts."yo-vessat.otawilma.fi" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://yo-vessat-otawilma";
          proxyWebsockets = true;
        };
      };

      virtualHosts."infotaulu.otawilma.fi" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://infotaulu-otawilma";
          proxyWebsockets = true;
        };
      };

      virtualHosts."kartat.otawilma.fi" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://kartat-otawilma";
          proxyWebsockets = true;
        };
      };

      virtualHosts."otawilma.fi" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://otawilma";
          proxyWebsockets = true;
        };
      };

      virtualHosts."api.otawilma.fi" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://api-otawilma";
          proxyWebsockets = true;
        };
      };

      virtualHosts."wilma.otawilma.fi" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://wilma-otawilma";
          proxyWebsockets = true;
        };
      };

      virtualHosts."rannasta-suomeen.fi" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://rannasta-suomeen";
          proxyWebsockets = true;
        };
      };

      virtualHosts."api.rannasta-suomeen.fi" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://api-rannasta-suomeen";
          proxyWebsockets = true;
        };
      };

      virtualHosts."cdn.rannasta-suomeen.fi" = {
        enableACME = true;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://cdn-rannasta-suomeen";
          proxyWebsockets = true;
        };
      };

      streamConfig = ''
        upstream db-rannasta-suomeen {
            server 10.128.0.2:5432;
        }
        server {
            listen 0.0.0.0:5432 so_keepalive=on;
            proxy_pass db-rannasta-suomeen;
        }
      '';

      upstreams.otawilma.servers."10.128.0.2:9302" = { };
      upstreams.api-otawilma.servers."10.128.0.2:9303" = { };
      upstreams.wilma-otawilma.servers."10.128.0.2:9301" = { };
      upstreams.kartat-api-otawilma.servers."10.128.0.2:3001" = { };
      upstreams.kartat-otawilma.servers."10.128.0.2:9304" = { };

      upstreams.yo-vessat-otawilma.servers."10.128.0.2:9305" = { };
      upstreams.infotaulu-otawilma.servers."10.128.0.2:3060" = { };
      upstreams.prometheus-otawilma.servers."10.128.0.2:9090" = { };

      upstreams.rannasta-suomeen.servers."10.128.0.2:3030" = { };
      upstreams.api-rannasta-suomeen.servers."10.128.0.2:8000" = { };
      upstreams.cdn-rannasta-suomeen.servers."10.128.0.2:3050" = { };

      upstreams.fablab-kube.servers = builtins.listToAttrs (
        builtins.map
          (name: {
            name = "${name}:443";
            value = { };
          })
          [
            "10.42.0.11"
            "10.42.0.12"
            "10.42.0.13"
            "10.42.0.15"
            "10.42.0.16"
            "10.42.0.17"
            "10.42.0.18"
            "10.42.0.19"
          ]
      );
    };

    openssh.enable = true;
    openssh.settings.PasswordAuthentication = false;

    resolved.enable = true;
  };
}
