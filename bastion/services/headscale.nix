{ config, ... }:
let
  cfg = config.services.headscale;
in
{
  services.headscale = {
    enable = true;
    port = 1194; # openvpn port number
    settings = {
      server_url = "https://vpn.otaniemi.org";
      ip_prefixes = [
        "fd7a:115c:a1e0::/48"
        "100.68.0.0/20"
      ];
      # New configuration format
      # prefixes = {
      #   v4 = "100.68.0.0/20";
      #   allocation = "sequential";
      # };
      disable_check_updates = true;

      # https://tailscale.com/kb/1181/firewalls#fortinet
      randomize_client_port = true;

      dns.base_domain = "hakkeriverkko.internal";
      dns.extra_records = [
        {
          name = "ctfd.hakkeriverkko.internal";
          type = "A";
          value = "100.68.0.1";
        }
        {
          name = "vaultwarden.hakkeriverkko.internal";
          type = "A";
          value = "100.68.0.1";
        }
        {
          name = "juice.hakkeriverkko.internal";
          type = "A";
          value = "10.168.0.47";
        }
      ];
    };
  };

  services.nginx.virtualHosts."vpn.otaniemi.org" = {
    enableACME = true;
    addSSL = true;
    extraConfig = ''
      proxy_buffering off;
      add_header Strict-Transport-Security "max-age=15552000; includeSubDomains" always;
    '';
    locations."/" = {
      proxyPass = "http://127.0.0.1:${toString cfg.port}";
      proxyWebsockets = true;
    };
  };
}
