{ pkgs, config, ... }:
let
  port = 8463;
in
{
  services.nginx = {
    virtualHosts."tt.otaniemi.org" = {
      locations."/authenticate" = {
        proxyPass = "http://tt-authenticate";
      };
    };

    upstreams.tt-authenticate.servers."127.0.0.1:${toString port}" = { };
  };

  # Follow the logs to register the nodes:
  #
  #   journalctl -fu infosec-authenticate |grep nodekey
  systemd.services.infosec-authenticate = {
    enable = true;
    serviceConfig = {
      Type = "simple";
    };
    wantedBy = [ "multi-user.target" ];
    script = ''
      while true; do echo -e "HTTP/1.1 200 OK\r\n\r\n $(date)" | ${pkgs.netcat}/bin/nc -nvlN ${toString port}; done
    '';
  };
}
