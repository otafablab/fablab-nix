{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:
let
  port = 8001;
in
{
  virtualisation.podman.defaultNetwork.settings.dns_enabled = true;
  # virtualisation.containers.containersConf.cniPlugins = [
  # TODO nftables plugin https://github.com/greenpau/cni-plugins    
  # ]

  # virtualisation.oci-containers.containers.constructive-logic-course-lean4game = {
  #   image = "ghcr.io/xhalo32/lean4game/client-proxy:latest";
  #   extraOptions = [
  #     "--publish=${toString port}:80"
  #   ];
  # };

  # Logic disabled
  # services.nginx.virtualHosts."logic.fablab-systems.fi" = {
  #   useACMEHost = "fablab-systems.fi";
  #   addSSL = true;
  #   extraConfig = ''
  #     proxy_buffering off;
  #     add_header Strict-Transport-Security "max-age=15552000; includeSubDomains" always;
  #   '';
  #   locations."/data/" = {
  #     proxyPass = "https://adam.math.hhu.de/data/";
  #     recommendedProxySettings = false; # Host needs to be manually configured
  #     extraConfig = ''
  #       proxy_set_header Host "adam.math.hhu.de";
  #       proxy_set_header        X-Real-IP $remote_addr;
  #       proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
  #       proxy_set_header        X-Forwarded-Proto $scheme;
  #       proxy_set_header        X-Forwarded-Host $host;
  #       proxy_set_header        X-Forwarded-Server $host;
  #     '';
  #   };
  #   locations."/websocket/" = {
  #     proxyPass = "https://adam.math.hhu.de/websocket/";
  #     recommendedProxySettings = false; # Host needs to be manually configured
  #     extraConfig = ''
  #       proxy_set_header Host "adam.math.hhu.de";
  #       proxy_set_header        X-Real-IP $remote_addr;
  #       proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
  #       proxy_set_header        X-Forwarded-Proto $scheme;
  #       proxy_set_header        X-Forwarded-Host $host;
  #       proxy_set_header        X-Forwarded-Server $host;
  #     '';
  #     proxyWebsockets = true;
  #   };
  #   locations."/" = {
  #     root = inputs.lean4game.packages.x86_64-linux.lean4game;
  #   };
  # };
}
