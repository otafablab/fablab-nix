#!/usr/bin/env sh
set -e
#set -x

pan=0
tilt=0

function control_camera {
    nix run nixpkgs#uvcdynctrl -- -d video2 -s "$1"', Absolute' -- "$2" 2>&-
}

function tilt {
    tilt=$(echo "$tilt" "$1" |awk '{printf "%f", $1 + $2 * 4000}')
    control_camera Tilt "$tilt"
}

function pan {
    pan=$(echo "$pan" "$1" |awk '{printf "%f", $1 + $2 * 4000}')
    control_camera Pan "$pan"
}

echo "Use arrow keys to control camera. Press Ctrl-C to exit"

while true
do
    read -rsn 1 t
    case $t in
        A) tilt -5 ;;
        B) tilt 5 ;;
        C) pan -5 ;;
        D) pan 5 ;;
    esac
done
