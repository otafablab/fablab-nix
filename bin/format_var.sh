#!/usr/bin/env sh

[ "$1" == "" ] && { echo "Usage: format_var.sh DEVICE"; exit 1; }

nix run nixpkgs#parted -- -s "$1" -- mklabel gpt
nix run nixpkgs#parted -- -s "$1" -- mkpart primary 0% 100%
mkfs.ext4 -L var "${1}p1"
