#!/usr/bin/env sh
{pkgs}:
pkgs.writeShellScriptBin "hw-info" ''
  set -euo pipefail
  echo "-----BEGIN KERNEL INFO-----"
  uname -a
  echo "-----END KERNEL INFO-----"
  echo ""
  echo "-----BEGIN KERNEL COMMAND LINE-----"
  cat /proc/cmdline
  echo "-----END KERNEL COMMAND LINE-----"
  echo ""
  echo "-----BEGIN CPU CORE COUNT AND MODEL-----"
  grep '^model name' /proc/cpuinfo | cut -d: -f 2- | sort | uniq -c
  echo "-----BEGIN CPU CORE COUNT AND MODEL-----"
  echo ""
  echo "-----BEGIN PCI INFO-----"
  ${pkgs.pciutils}/bin/lspci -nn
  echo "-----END PCI INFO-----"
  echo ""
  echo "-----BEGIN USB INFO-----"
  ${pkgs.usbutils}/bin/lsusb
  echo "-----END USB INFO-----"
  echo ""
  echo "-----BEGIN MODALIASES-----"
  find /sys/devices/ -name modalias -print0 2> /dev/null | xargs -0 cat \
      | sort | uniq -c
  echo "-----END MODALIASES-----"
  echo ""
  echo "-----BEGIN SERIAL PORTS-----"
  find /sys/class/tty/ ! -type d -print0 2> /dev/null \
      | xargs -0 readlink -f \
      | sort -u | { grep -E -v 'devices/virtual|devices/platform' || true; }
  echo "-----END SERIAL PORTS-----"
  echo ""
  echo "-----BEGIN NETWORK INTERFACES-----"
  ${pkgs.iproute2}/bin/ip -o link
  echo "-----END NETWORK INTERFACES-----"
  echo ""
  echo "-----BEGIN BLOCK DEVICE SUMMARY-----"
  # Note: excluding ramdisks, floppy drives, and loopback devices.
  lsblk --exclude 1,2,7 -o NAME,MAJ:MIN,FSTYPE,PHY-SEC,SIZE,VENDOR,MODEL
  echo "-----END BLOCK DEVICE SUMMARY-----"
  # The remainder of this script only runs as root (during commissioning).

  if [ "$(id -u)" != "0" ]; then
      # Do not fail commissioning if this fails.
      echo "[*] Exiting gracefully due to user not being root" >&2
      exit 0
  fi
  # via http://git.savannah.nongnu.org/cgit/dmidecode.git/tree/dmiopt.c
  DMI_STRINGS="
          bios-vendor
          bios-version
          bios-release-date
          system-manufacturer
          system-product-name
          system-version
          system-serial-number
          system-uuid
          baseboard-manufacturer
          baseboard-product-name
          baseboard-version
          baseboard-serial-number
          baseboard-asset-tag
          chassis-manufacturer
          chassis-type
          chassis-version
          chassis-serial-number
          chassis-asset-tag
          processor-family
          processor-manufacturer
          processor-version
          processor-frequency
      "
  echo ""
  echo "-----BEGIN DMI KEYPAIRS-----"
  for key in $DMI_STRINGS; do
      value=$(${pkgs.dmidecode}/bin/dmidecode -s "$key" |tr '\n' ',' |sed 's/,$//')
      printf "%s=%s\\n" "$key" "$value"
  done
  echo "-----END DMI KEYPAIRS-----"
''
