#!/usr/bin/env sh
{pkgs, ...}:
pkgs.writeShellScriptBin "setupVar" ''
  set -xeuo pipefail

  ${pkgs.parted}/bin/parted --script "''$1" \
      mklabel gpt \
      mkpart var ext4 1MiB 100%
  mkfs.ext4 -L var "''${1}1"
  mount "''${1}1" /var
  systemctl restart sshd
''
