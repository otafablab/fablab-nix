#!/usr/bin/env sh
{pkgs}:
pkgs.writeShellScriptBin "upgrade" ''
  systemctl restart nixos-upgrade &journalctl -fu nixos-upgrade
''
