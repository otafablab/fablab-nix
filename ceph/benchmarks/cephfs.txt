bench-n2hjc
seqread: (g=0): rw=read, bs=(R) 512KiB-512KiB, (W) 512KiB-512KiB, (T) 512KiB-512KiB, ioengine=psync, iodepth=10
...
seqwrite: (g=0): rw=write, bs=(R) 512KiB-512KiB, (W) 512KiB-512KiB, (T) 512KiB-512KiB, ioengine=psync, iodepth=10
...
fio-3.19
Starting 4 processes
seqread: Laying out IO file (1 file / 10MiB)
seqread: Laying out IO file (1 file / 10MiB)
seqwrite: Laying out IO file (1 file / 10MiB)
seqwrite: Laying out IO file (1 file / 10MiB)

seqread: (groupid=0, jobs=4): err= 0: pid=18: Tue Jan  9 22:15:08 2024
  read: IOPS=194, BW=97.2MiB/s (102MB/s)(973MiB/10009msec)
    clat (usec): min=5685, max=69070, avg=10281.89, stdev=3323.70
     lat (usec): min=5685, max=69070, avg=10282.09, stdev=3323.72
    clat percentiles (usec):
     |  1.00th=[ 8717],  5.00th=[ 8848], 10.00th=[ 8979], 20.00th=[ 8979],
     | 30.00th=[ 8979], 40.00th=[ 8979], 50.00th=[ 8979], 60.00th=[ 8979],
     | 70.00th=[10552], 80.00th=[11731], 90.00th=[13304], 95.00th=[13566],
     | 99.00th=[26084], 99.50th=[34341], 99.90th=[64750], 99.95th=[68682],
     | 99.99th=[68682]
   bw (  KiB/s): min=57281, max=105472, per=100.00%, avg=100510.37, stdev=5198.34, samples=38
   iops        : min=  111, max=  206, avg=196.26, stdev=10.25, samples=38
  write: IOPS=21, BW=10.6MiB/s (11.1MB/s)(107MiB/10049msec); 0 zone resets
    clat (msec): min=74, max=184, avg=94.05, stdev=13.09
     lat (msec): min=74, max=184, avg=94.05, stdev=13.09
    clat percentiles (msec):
     |  1.00th=[   77],  5.00th=[   82], 10.00th=[   82], 20.00th=[   86],
     | 30.00th=[   90], 40.00th=[   90], 50.00th=[   94], 60.00th=[   95],
     | 70.00th=[  100], 80.00th=[  100], 90.00th=[  100], 95.00th=[  108],
     | 99.00th=[  157], 99.50th=[  163], 99.90th=[  184], 99.95th=[  184],
     | 99.99th=[  184]
   bw (  KiB/s): min= 8355, max=12288, per=99.98%, avg=10850.11, stdev=607.26, samples=38
   iops        : min=   15, max=   24, avg=21.16, stdev= 1.22, samples=38
  lat (msec)   : 10=60.21%, 20=28.72%, 50=1.11%, 100=8.99%, 250=0.97%
  cpu          : usr=0.05%, sys=0.17%, ctx=2166, majf=0, minf=306
  IO depths    : 1=100.0%, 2=0.0%, 4=0.0%, 8=0.0%, 16=0.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued rwts: total=1946,213,0,0 short=0,0,0,0 dropped=0,0,0,0
     latency   : target=0, window=0, percentile=100.00%, depth=10

Run status group 0 (all jobs):
   READ: bw=97.2MiB/s (102MB/s), 97.2MiB/s-97.2MiB/s (102MB/s-102MB/s), io=973MiB (1020MB), run=10009-10009msec
  WRITE: bw=10.6MiB/s (11.1MB/s), 10.6MiB/s-10.6MiB/s (11.1MB/s-11.1MB/s), io=107MiB (112MB), run=10049-10049msec
