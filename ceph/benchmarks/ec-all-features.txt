bench-slrvp
seqread: (g=0): rw=read, bs=(R) 512KiB-512KiB, (W) 512KiB-512KiB, (T) 512KiB-512KiB, ioengine=psync, iodepth=10
...
seqwrite: (g=0): rw=write, bs=(R) 512KiB-512KiB, (W) 512KiB-512KiB, (T) 512KiB-512KiB, ioengine=psync, iodepth=10
...
fio-3.19
Starting 4 processes
seqread: Laying out IO file (1 file / 10MiB)
seqread: Laying out IO file (1 file / 10MiB)
seqwrite: Laying out IO file (1 file / 10MiB)
seqwrite: Laying out IO file (1 file / 10MiB)

seqread: (groupid=0, jobs=4): err= 0: pid=18: Tue Jan  9 20:41:35 2024
  read: IOPS=162, BW=81.4MiB/s (85.4MB/s)(815MiB/10006msec)
    clat (msec): min=7, max=109, avg=12.28, stdev= 6.44
     lat (msec): min=7, max=109, avg=12.28, stdev= 6.44
    clat percentiles (msec):
     |  1.00th=[    9],  5.00th=[   10], 10.00th=[   10], 20.00th=[   11],
     | 30.00th=[   11], 40.00th=[   11], 50.00th=[   11], 60.00th=[   11],
     | 70.00th=[   11], 80.00th=[   13], 90.00th=[   18], 95.00th=[   21],
     | 99.00th=[   29], 99.50th=[   57], 99.90th=[  102], 99.95th=[  110],
     | 99.99th=[  110]
   bw (  KiB/s): min=49367, max=92160, per=100.00%, avg=83871.53, stdev=5128.29, samples=38
   iops        : min=   95, max=  180, avg=163.74, stdev=10.14, samples=38
  write: IOPS=19, BW=9.82MiB/s (10.3MB/s)(98.5MiB/10026msec); 0 zone resets
    clat (msec): min=67, max=197, avg=101.69, stdev=20.38
     lat (msec): min=67, max=197, avg=101.69, stdev=20.38
    clat percentiles (msec):
     |  1.00th=[   68],  5.00th=[   73], 10.00th=[   82], 20.00th=[   85],
     | 30.00th=[   92], 40.00th=[   95], 50.00th=[   99], 60.00th=[  104],
     | 70.00th=[  109], 80.00th=[  112], 90.00th=[  126], 95.00th=[  134],
     | 99.00th=[  184], 99.50th=[  199], 99.90th=[  199], 99.95th=[  199],
     | 99.99th=[  199]
   bw (  KiB/s): min= 8101, max=12288, per=98.90%, avg=9949.32, stdev=594.53, samples=38
   iops        : min=   14, max=   24, avg=19.37, stdev= 1.21, samples=38
  lat (msec)   : 10=17.20%, 20=66.81%, 50=4.71%, 100=5.91%, 250=5.37%
  cpu          : usr=0.11%, sys=0.15%, ctx=1828, majf=0, minf=310
  IO depths    : 1=100.0%, 2=0.0%, 4=0.0%, 8=0.0%, 16=0.0%, 32=0.0%, >=64=0.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     issued rwts: total=1629,197,0,0 short=0,0,0,0 dropped=0,0,0,0
     latency   : target=0, window=0, percentile=100.00%, depth=10

Run status group 0 (all jobs):
   READ: bw=81.4MiB/s (85.4MB/s), 81.4MiB/s-81.4MiB/s (85.4MB/s-85.4MB/s), io=815MiB (854MB), run=10006-10006msec
  WRITE: bw=9.82MiB/s (10.3MB/s), 9.82MiB/s-9.82MiB/s (10.3MB/s-10.3MB/s), io=98.5MiB (103MB), run=10026-10026msec

Disk stats (read/write):
  rbd0: ios=1661/201, merge=0/6, ticks=22907/20053, in_queue=42961, util=99.00%
