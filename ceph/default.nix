{pkgs, ...}: {
  imports = [
    ../modules/ceph.nix
    ./mon.nix
    ./osd.nix
    ./mds.nix
  ];
  fablab.services.ceph = {
    # FIXME cephadm ls
    # FileNotFoundError: [Errno 2] No such file or directory: '/etc/logrotate.d/cephadm'
    enable = true;
    global = {
      monInitialMembers = "edelman fredkin reynolds";
      # monHost = builtins.foldl' (acc: e: acc ++ ["[v2:${e}:3300/0,v1:${e}:6789/0] "]) ["10.42.0.15" "10.42.0.16"];
      monHost = "10.42.0.15,10.42.0.16,10.42.0.27";
      fsid = "7ca0d35d-c5ac-4620-8d1a-9284f2adcd09";
      publicNetwork = "10.42.0.0/24";
      clusterName = "ceph"; # https://github.com/NixOS/nixpkgs/blob/nixos-unstable/nixos/modules/services/network-filesystems/ceph.nix#L351 needs to have <cluster name>.conf
      authServiceRequired = "none";
      authClusterRequired = "none";
      authClientRequired = "none";
    };
    extraConfig = {
      # "debug ms" = "1/5"; # set messenger debug
    };
  };
  networking.firewall.allowedTCPPorts = [
    6789 # mon v1
    3300 # mon v2
  ];
  networking.firewall.allowedTCPPortRanges = [
    {
      from = 6800;
      to = 7100;
    }
  ];
  environment.systemPackages = with pkgs; [ceph];
}
