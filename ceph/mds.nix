{config, ...}: {
  fablab.services.ceph.mds = {
    enable = true;
    daemons = [config.networking.hostName];
  };
}
