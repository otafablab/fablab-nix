{config, ...}: {
  fablab.services.ceph = {
    mon = {
      enable = builtins.elem config.networking.hostName (builtins.split " " config.fablab.services.ceph.global.monInitialMembers);
      # Run one monitor on this host
      daemons = [config.networking.hostName];
      extraConfig = {
        "mgr initial modules" = "dashboard balancer";
        "auth allow insecure global id reclaim" = "false";
        "debug mon" = "1"; # TODO
        "debug paxos" = "1/5";
      };
    };
    # Set up one manager on each monitor node
    # https://docs.ceph.com/en/latest/install/manual-deployment/#manager-daemon-configuration
    mgr = {
      inherit (config.fablab.services.ceph.mon) enable daemons;
    };
  };
}
