{config, ...}: let
  mapping = {
    edelman = [
      {
        id = 0;
        diskId = "ata-WDC_WD5003ABYX-50WERA1_WD-WMAYP5372447";
        # enable = false; to disable the osd service
      }
    ];
    fredkin = [
      {
        id = 1;
        diskId = "ata-ST500DM002-1BD142_Z6EB0L8F";
      }
    ];
    ousterhout = [
      {
        id = 4;
        diskId = "ata-Fanxiang_S101_128GB_AA000000000000001210";
      }
    ];
    # peyton-jones = [7 8 9];
    reynolds = [
      {
        id = 10;
        diskId = "ata-Fanxiang_S101_128GB_AA000000000000001474";
      }
      {
        id = 11;
        diskId = "ata-Fanxiang_S101_128GB_AA000000000000001359";
      }
      {
        id = 12;
        diskId = "ata-INTENSO_AA000000000000001519";
      }
    ];
  };
  name = config.networking.hostName;
in {
  fablab.services.ceph.osd = {
    enable = builtins.hasAttr name mapping;
    daemons =
      if builtins.hasAttr name mapping
      then mapping.${name}
      else [];
  };
}
