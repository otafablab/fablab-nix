# This flake is currently used only for the raspberry pis
{
  description = "Fablab NixOS configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-parts.url = "github:hercules-ci/flake-parts";
    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    microvm = {
      url = "github:astro/microvm.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    lean4game = {
      url = "github:xhalo32/lean4game";
      # inputs.nixpkgs.follows = "nixpkgs"; # We don't to rebuild lean4game when nixpkgs changes
    };
    wiki = {
      url = "gitlab:otafablab/wiki";
      flake = false;
    };
    infosec-course = {
      url = "gitlab:otafablab/infosec-course";
    };
  };

  outputs =
    {
      nixpkgs,
      flake-parts,
      sops-nix,
      ...
    }@inputs:
    flake-parts.lib.mkFlake { inherit inputs; } {
      systems = [ "x86_64-linux" ];

      flake = {
        nixosModules = {
          utils = import ./modules/utils.nix;
        };

        nixosConfigurations = (import ./rpi inputs) // {
          rtr = nixpkgs.lib.nixosSystem {
            specialArgs = {
              inherit inputs;
            };
            modules = [
              {
                nixpkgs.config.allowUnfree = true;
                nixpkgs.config.nvidia.acceptLicense = true;
              }
              sops-nix.nixosModules.sops
              ./rtr/configuration.nix
              {
                networking.hostName = "rtr";
                nixpkgs.hostPlatform = "x86_64-linux";
              }
            ];
          };
          bastion = nixpkgs.lib.nixosSystem {
            specialArgs = {
              inherit inputs;
            };
            modules = [
              sops-nix.nixosModules.sops
              ./bastion/configuration.nix
              {
                networking.hostName = "bastion";
                nixpkgs.hostPlatform = "x86_64-linux";
              }
            ];
          };

          ultimaker-box = nixpkgs.lib.nixosSystem {
            specialArgs = {
              inherit inputs;
            };
            modules = [
              # {
              #   nixpkgs.config.allowUnfree = true;
              #   nixpkgs.config.nvidia.acceptLicense = true;
              # }
              # sops-nix.nixosModules.sops
              ./ultimaker-box/configuration.nix
              {
                networking.hostName = "ultimaker-box";
                nixpkgs.hostPlatform = "x86_64-linux";
              }
            ];
          };

          git = nixpkgs.lib.nixosSystem {
            specialArgs = {
              inherit inputs;
            };
            modules = [
              sops-nix.nixosModules.sops
              ./git/configuration.nix
              {
                networking.hostName = "git";
                nixpkgs.hostPlatform = "x86_64-linux";
              }
            ];
          };

          ###################### OLD ###########################

          # amdahl = nixpkgs.lib.nixosSystem {
          #   specialArgs = {
          #     inherit inputs;
          #   };
          #   modules = [
          #     sops-nix.nixosModules.sops
          #     ./kube/configuration.nix
          #     ./kube/control-plane-node.nix
          #     {
          #       networking.hostName = "amdahl";
          #       nixpkgs.hostPlatform = "x86_64-linux";
          #     }
          #   ];
          # };
          # babbage = nixpkgs.lib.nixosSystem {
          #   specialArgs = {
          #     inherit inputs;
          #   };
          #   modules = [
          #     sops-nix.nixosModules.sops
          #     ./kube/configuration.nix
          #     ./kube/control-plane-node.nix

          #     {
          #       networking.hostName = "babbage";
          #       nixpkgs.hostPlatform = "x86_64-linux";
          #     }
          #   ];
          # };
          # carmack = nixpkgs.lib.nixosSystem {
          #   specialArgs = {
          #     inherit inputs;
          #   };
          #   modules = [
          #     sops-nix.nixosModules.sops
          #     ./kube/configuration.nix
          #     ./kube/control-plane-node.nix

          #     {
          #       networking.hostName = "carmack";
          #       nixpkgs.hostPlatform = "x86_64-linux";
          #     }
          #   ];
          # };
          # diffie -> lean-server
          # diffie = nixpkgs.lib.nixosSystem {
          #   specialArgs = {
          #     inherit inputs;
          #   };
          #   modules = [
          #     sops-nix.nixosModules.sops
          #     ./kube/configuration.nix
          #     ./kube/data-plane-node.nix

          #     {
          #       networking.hostName = "diffie";
          #       nixpkgs.hostPlatform = "x86_64-linux";
          #     }
          #   ];
          # };
          # edelman -> tietoturva-box
          # edelman = nixpkgs.lib.nixosSystem {
          #   specialArgs = {
          #     inherit inputs;
          #   };
          #   modules = [
          #     sops-nix.nixosModules.sops
          #     ./kube/configuration.nix
          #     ./kube/data-plane-node.nix
          #     # ./ceph # Disabled for now
          #     {
          #       networking.hostName = "edelman";
          #       nixpkgs.hostPlatform = "x86_64-linux";
          #     }
          #   ];
          # };
          # fredkin = nixpkgs.lib.nixosSystem {
          #   specialArgs = {
          #     inherit inputs;
          #   };
          #   modules = [
          #     sops-nix.nixosModules.sops
          #     ./kube/configuration.nix
          #     ./kube/data-plane-node.nix
          #     # ./ceph # Disabled for now
          #     {
          #       networking.hostName = "fredkin";
          #       nixpkgs.hostPlatform = "x86_64-linux";
          #     }
          #   ];
          # };
          # Moved to infosec course
          # gabriel = nixpkgs.lib.nixosSystem {
          #   specialArgs = {
          #     inherit inputs;
          #   };
          #   modules = [
          #     sops-nix.nixosModules.sops
          #     ./kube/configuration.nix
          #     ./kube/data-plane-node.nix

          #     {
          #       networking.hostName = "gabriel";
          #       nixpkgs.hostPlatform = "x86_64-linux";
          #     }
          #   ];
          # };
          # hellman = nixpkgs.lib.nixosSystem {
          #   specialArgs = {
          #     inherit inputs;
          #   };
          #   modules = [
          #     sops-nix.nixosModules.sops
          #     ./kube/configuration.nix
          #     ./kube/data-plane-node.nix

          #     {
          #       networking.hostName = "hellman";
          #       nixpkgs.hostPlatform = "x86_64-linux";
          #     }
          #   ];
          # };
          # Nvidia settings broken
          # iverson = nixpkgs.lib.nixosSystem {
          #   specialArgs = {
          #     inherit inputs;
          #   };
          #   modules = [
          #     (
          #       { modulesPath, ... }:
          #       {
          #         imports = [
          #           (modulesPath + "/installer/netboot/netboot.nix")
          #           (modulesPath + "/profiles/all-hardware.nix")
          #         ];
          #         nixpkgs.config.allowUnfree = true;
          #         nixpkgs.config.nvidia.acceptLicense = true;
          #       }
          #     )
          #     sops-nix.nixosModules.sops
          #     ./modules/admins.nix
          #     ./modules/core.nix
          #     ./modules/autoUpdate.nix
          #     ./modules/fileSystems.nix
          #     ./modules/openssh.nix
          #     ./modules/joonas.nix
          #     ./modules/nvidia.nix
          #     {
          #       networking.hostName = "iverson";
          #       nixpkgs.hostPlatform = "x86_64-linux";
          #     }
          #   ];
          # };

          # ousterhout = nixpkgs.lib.nixosSystem {
          #   specialArgs = {
          #     inherit inputs;
          #   };
          #   modules = [
          #     sops-nix.nixosModules.sops
          #     ./hosting
          #     ./ceph
          #     {
          #       networking.hostName = "ousterhout";
          #       nixpkgs.hostPlatform = "x86_64-linux";
          #     }
          #   ];
          # };
          # peyton-jones = nixpkgs.lib.nixosSystem {
          #   specialArgs = {
          #     inherit inputs;
          #   };
          #   modules = [
          #     sops-nix.nixosModules.sops
          #     ./localhost.nix
          #     ./modules/garage.nix
          #     {
          #       networking.hostName = "peyton-jones";
          #       nixpkgs.hostPlatform = "x86_64-linux";
          #     }
          #   ];
          # };
          # reynolds = nixpkgs.lib.nixosSystem {
          #   specialArgs = {
          #     inherit inputs;
          #   };
          #   modules = [
          #     sops-nix.nixosModules.sops
          #     ./ceph
          #     ./localhost.nix
          #     {
          #       networking.hostName = "reynolds";
          #       nixpkgs.hostPlatform = "x86_64-linux";
          #     }
          #   ];
          # };
          # shamir = nixpkgs.lib.nixosSystem {
          #   specialArgs = {
          #     inherit inputs;
          #   };
          #   modules = [
          #     sops-nix.nixosModules.sops
          #     ./localhost.nix
          #     {
          #       networking.hostName = "shamir";
          #       nixpkgs.hostPlatform = "x86_64-linux";
          #     }
          #   ];
          # };
          # thompson = nixpkgs.lib.nixosSystem {
          #   specialArgs = {
          #     inherit inputs;
          #   };
          #   modules = [
          #     sops-nix.nixosModules.sops
          #     ./localhost.nix
          #     {
          #       networking.hostName = "thompson";
          #       nixpkgs.hostPlatform = "x86_64-linux";
          #     }
          #   ];
          # };

          "localhost.localdomain" = nixpkgs.lib.nixosSystem {
            specialArgs = {
              inherit inputs;
            };
            modules = [
              ./localhost.nix
              {
                # hostName can be assigned from DHCP later
                networking.hostName = "";
                nixpkgs.hostPlatform = "x86_64-linux";
              }
            ];
          };

          # kubechad = nixpkgs.lib.nixosSystem {
          #   modules = [
          #     sops-nix.nixosModules.sops
          #     microvm.nixosModules.microvm
          #     ./kube/chad.nix
          #   ];
          # };
        };
      };

      perSystem =
        { pkgs, system, ... }:
        {
          packages = {
            hw-info = import ./bin/hw-info.nix { inherit pkgs; };
            upgrade = import ./bin/upgrade.nix { inherit pkgs; };
            setupVar = import ./bin/setupVar.nix { inherit pkgs; };
            nixos-installer = pkgs.writeShellScriptBin "nixos-installer" (
              builtins.readFile ./bin/nixos-installer
            );
            # kubechad = flake.nixosConfigurations.kubechad.config.microvm.declaredRunner;
          };
          devShells.cuda = import ./shells/cuda-fhs.nix {
            pkgs = import nixpkgs {
              inherit system;
              config.allowUnfree = true;
              config.nvidia.acceptLicense = true;
            };
          };
          #   devenv.shells.default =
          #     let
          #       deploy = pkgs.writeShellScriptBin "deploy" ''
          #         [[ "$1" == "" ]] && { echo "Usage: deploy OUTPUT_NAME"; exit 1; }
          #         ${pkgs.vals}/bin/vals eval <$(nix build ".#$1" --print-out-paths) |${pkgs.kapp}/bin/kapp deploy -a "$1" -n default -yf-
          #       '';
          #       # Use with `plan fablab-management` inside kube directory
          #       plan = pkgs.writeShellScriptBin "plan" ''
          #         [[ "$1" == "" ]] && { echo "Usage: plan OUTPUT_NAME"; exit 1; }
          #         ${pkgs.vals}/bin/vals eval <$(nix build ".#$1" --print-out-paths) |${pkgs.kapp}/bin/kapp deploy -a "$1" -n default --diff-run -c -f-
          #       '';
          #     in
          #     {
          #       pre-commit = {
          #         hooks = {
          #           # FIXME These only cause pain
          #           # alejandra.enable = true;
          #           # deadnix.enable = true;
          #           # nil.enable = true;
          #           # statix.enable = true;
          #         };
          #         settings = {
          #           deadnix = {
          #             edit = true;
          #             hidden = true;
          #           };
          #         };
          #       };
          #       packages = with pkgs; [
          #         kubectl
          #         krew
          #         pssh
          #         kapp
          #         vals
          #         deploy
          #         plan
          #       ];
          #       # TODO: Remove when https://github.com/cachix/devenv/issues/760 is fixed
          #       containers = pkgs.lib.mkForce { };
          #     };
        };
    };
}
