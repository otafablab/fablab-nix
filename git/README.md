# Git server

## Web endpoints

- <https://git.fablab-systems.fi>

## Specification

- Runs sourcehut
    - Future git server for all fablab related projects.
      > Public GitLab is too restricted in its capabilities.

### Hardware

Product: `HP Z240 Tower Workstation`

Serial: `CZC7468G3R`

```
8  Intel(R) Xeon(R) CPU E3-1230 v5 @ 3.40GHz
```

```
sda           8:0              512 465.8G ATA      WDC WD5003ABYX-50WERA1
nvme0n1     259:0              512 476.9G          SAMSUNG MZVLW512HMJP-000H1
```
