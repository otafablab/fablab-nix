{ config, pkgs, ... }:
{
  imports = [
    ./hardware-configuration.nix
    ../modules/admins.nix
    ../modules/autoUpdate.nix
    ../modules/core.nix
    ./services
  ];

  sops = {
    defaultSopsFile = ../secrets/${config.networking.hostName}.yaml;
    secrets."cloudflare/lego-env" = {
      owner = "acme";
      group = "acme";
    };
    secrets."srht/network-key" = { };
    secrets."srht/service-key" = { };
  };

  environment.defaultPackages = with pkgs; [ python3 ];

  services.openssh.enable = true;
  services.openssh.settings.PasswordAuthentication = false;

  system.stateVersion = "24.05";
}
