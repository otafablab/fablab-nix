{ config, ... }:
{
  security.acme = {
    acceptTerms = true;
    defaults.email = "niklas.halonen@protonmail.com";
    defaults.group = "nginx";
    certs."fablab-systems.fi" = {
      # Comment out for production server after testing with staging
      # server = "https://acme-staging-v02.api.letsencrypt.org/directory";
      dnsProvider = "cloudflare";
      extraDomainNames = [ "*.fablab-systems.fi" ];
      environmentFile = config.sops.secrets."cloudflare/lego-env".path;
    };
  };
}
