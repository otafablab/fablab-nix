{
  config,
  pkgs,
  lib,
  ...
}:
let
  fqdn = "git.fablab-systems.fi";
in
{
  networking.firewall.allowedTCPPorts = [
    22
    80
    443
  ];

  security.acme.certs."${fqdn}".extraDomainNames = [
    "meta.${fqdn}"
    "man.${fqdn}"
    "git.${fqdn}"
  ];

  # TODO redict is a fork of redis (mentioned on sr.ht man page)
  # services.redis.package = pkgs.redict;

  services.sourcehut = {
    enable = true;

    postgresql.enable = true;
    redis.enable = true;

    meta = {
      enable = true;
    };

    # need oauth client id first

    # man.enable = true;
    # git.enable = true;
    # builds.enable = true;

    settings = {
      "sr.ht" = {
        environment = "production";
        global-domain = fqdn;
        origin = "https://${fqdn}";

        network-key = config.sops.secrets."srht/network-key".path;
        service-key = config.sops.secrets."srht/service-key".path;
      };

      mail = {
        pgp-key-id = "key_id";
        pgp-privkey = "/etc/nixos/srht-key";
        pgp-pubkey = "/etc/nixos/pubkey.gpg";
        smtp-from = "srht@maildomain";
        smtp-host = "srht@maildomain";
      };
      webhooks.private-key = "/var/lib/sourcehut/webhooks.privkey";
    };

    nginx.virtualHost = {
      useACMEHost = "fablab-systems.fi";
      addSSL = true;
    };
  };

  services.nginx = {
    enable = true;
    # only recommendedProxySettings are strictly required, but the rest make sense as well.
    recommendedTlsSettings = true;
    recommendedOptimisation = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;

    # Settings to setup what certificates are used for which endpoint.
    virtualHosts = {
      "meta.${fqdn}".useACMEHost = "fablab-systems.fi";
      "man.${fqdn}".useACMEHost = "fablab-systems.fi";
      "git.${fqdn}".useACMEHost = "fablab-systems.fi";
    };
  };
}
