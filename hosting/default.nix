{ config, modulesPath, ... }: {
  imports = [
    (modulesPath + "/installer/netboot/netboot.nix")
    (modulesPath + "/profiles/all-hardware.nix")
    ../modules/admins.nix
    ../modules/core.nix
    ../modules/autoUpdate.nix
    ../modules/fileSystems.nix
    ../modules/openssh.nix
    ./lxd.nix
  ];

  networking.networkmanager.enable = true;

  # sops = {
  #   defaultSopsFile = ../secrets/todo.yaml;
  # };
  zramSwap.enable = true;

  system.stateVersion = config.system.nixos.release;
}
