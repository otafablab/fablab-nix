{config, ...}: let
  # FIXME
  listen_address = "10.42.0.25";
  listen_port = 8443;
in {
  networking.firewall.allowedTCPPorts = [listen_port];
  # LXD network: https://documentation.ubuntu.com/lxd/en/latest/howto/network_bridge_firewalld/
  networking.firewall.trustedInterfaces = ["lxdbr0"];
  boot.kernel.sysctl = {"net.ipv4.conf.all.forwarding" = true;};
  # TODO configure lxdbr0 with systemd.network.netdevs
  virtualisation.lxd = {
    # TODO
    # https://documentation.ubuntu.com/lxd/en/latest/installing/#machine-setup
    enable = true;
    ui.enable = true;
    recommendedSysctlSettings = true;
    preseed = {
      config = {
        "core.https_address" = "${listen_address}:${toString listen_port}";
        "core.trust_password" = "sekret";
        "images.auto_update_interval" = 15;
      };
      networks = [
        {
          name = "lxdbr0";
          type = "bridge";
          config = {
            "ipv4.address" = "10.52.0.1/24";
            "ipv4.nat" = "true";
            "ipv4.firewall" = false;
            "ipv6.firewall" = false;
          };
        }
      ];
      profiles = [
        {
          name = "default";
          devices = {
            eth0 = {
              name = "eth0";
              network = "lxdbr0";
              type = "nic";
            };
            root = {
              path = "/";
              pool = "default";
              size = "35GiB";
              type = "disk";
            };
          };
        }
      ];
      storage_pools = [
        {
          name = "default";
          driver = "dir";
          config = {
            source = "/var/lib/lxd/storage-pools/default";
          };
        }
      ];
    };
  };
}
