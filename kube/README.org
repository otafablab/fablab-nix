* Kubernetes based infrastructure and management
** Development environment quickstart
*** Start minikube
#+begin_src shell
minikube start --cpus 8 --memory 10g --driver kvm2 --extra-disks 2
#+end_src

*** Deploy scripts
The devenv profile includes two shell scripts: ~plan~ and ~deploy~.

**** Plan
Running

#+begin_src shell
plan fablab-management
#+end_src

Shows a diff of what deploying the kubenix output ~fablab-management~ will do.

***** Example output
#+begin_src shell
Changes

Namespace          Name                                    Kind                 Age  Op      Op st.  Wait to    Rs  Ri
(cluster)          fablab-management                       Namespace            18d  update  -       reconcile  ok  -
fablab-management  fablab.otaniemi.org-tls                 Secret               14d  update  -       reconcile  ok  -
^                  keycloak                                Ingress              18d  update  -       reconcile  ok  -
^                  keycloak                                PodDisruptionBudget  18d  update  -       reconcile  ok  -
^                  keycloak                                Secret               18d  update  -       reconcile  ok  -
^                  keycloak                                Service              18d  update  -       reconcile  ok  -
^                  keycloak                                StatefulSet          18d  update  -       reconcile  ok  -
^                  keycloak-env-vars                       ConfigMap            18d  update  -       reconcile  ok  -
^                  keycloak-headless                       Service              18d  update  -       reconcile  ok  -
^                  keycloak-keycloak-config-cli            Job                  -    create  -       reconcile  -   -
^                  keycloak-keycloak-config-cli-configmap  ConfigMap            18d  update  -       reconcile  ok  -
^                  keycloak-metrics                        Service              18d  update  -       reconcile  ok  -
^                  keycloak-postgresql                     Secret               18d  update  -       reconcile  ok  -
^                  keycloak-postgresql                     Service              18d  update  -       reconcile  ok  -
^                  keycloak-postgresql                     StatefulSet          18d  update  -       reconcile  ok  -
^                  keycloak-postgresql-hl                  Service              18d  update  -       reconcile  ok  -
^                  oauth2-proxy                            ConfigMap            18d  update  -       reconcile  ok  -
^                  oauth2-proxy                            Deployment           14d  update  -       reconcile  ok  -
^                  oauth2-proxy                            Ingress              14d  update  -       reconcile  ok  -
^                  oauth2-proxy                            Secret               18d  update  -       reconcile  ok  -
^                  oauth2-proxy                            Service              18d  update  -       reconcile  ok  -
^                  oauth2-proxy                            ServiceAccount       18d  update  -       reconcile  ok  -
^                  oauth2-proxy-redis                      Secret               18d  update  -       reconcile  ok  -
^                  oauth2-proxy-redis                      ServiceAccount       18d  update  -       reconcile  ok  -
^                  oauth2-proxy-redis-configuration        ConfigMap            18d  update  -       reconcile  ok  -
^                  oauth2-proxy-redis-headless             Service              18d  update  -       reconcile  ok  -
^                  oauth2-proxy-redis-health               ConfigMap            18d  update  -       reconcile  ok  -
^                  oauth2-proxy-redis-master               Service              18d  update  -       reconcile  ok  -
^                  oauth2-proxy-redis-master               StatefulSet          14d  update  -       reconcile  ok  -
^                  oauth2-proxy-redis-replicas             Service              14d  update  -       reconcile  ok  -
^                  oauth2-proxy-redis-replicas             StatefulSet          14d  update  -       reconcile  ok  -
^                  oauth2-proxy-redis-scripts              ConfigMap            18d  update  -       reconcile  ok  -
^                  oauth2-proxy-watch-redis                Role                 14d  update  -       reconcile  ok  -
^                  oauth2-proxy-watch-redis                RoleBinding          14d  update  -       reconcile  ok  -

Op:      1 create, 0 delete, 33 update, 0 noop, 0 exists
Wait to: 34 reconcile, 0 delete, 0 noop

Succeeded
#+end_src

**** Deploy

After checking the output of ~plan~, use the ~deploy~ script:

#+begin_src shell
deploy fablab-management
#+end_src

It will not ask for confirmation due to vals integration when decrypting secrets.

*** Deploy rook-ceph
Warning: destroys (empty?) disks connected to kubernetes nodes.

#+begin_src shell
nix build .#rook-ceph
kapp deploy -a rook-ceph -f result -n default
#+end_src

*** Deploy fablab-management
#+begin_src shell
vals eval <(nix build .#fablab-management --print-out-paths) |kapp deploy -a fablab-management -n default -yf-
#+end_src

Note: requires pgp key to decrypt secrets

When redeploying, old jobs should be destroyed first

#+begin_src shell
kubectl delete job --all -n fablab-management
#+end_src

*** Deploy cert-manager
#+begin_src shell
vals eval <(nix build .#cert-manager --print-out-paths) |kapp deploy -a cert-manager -n default -yf-
#+end_src

Also requires deleting old jobs

*** Debugging rook
Deploy the toolbox
#+begin_src shell
kapp deploy -a toolbox -f https://raw.githubusercontent.com/rook/rook/master/deploy/examples/toolbox.yaml
#+end_src

Run ceph commands inside toolbox
#+begin_src shell
kubectl exec -it -n rook-ceph deploy/rook-ceph-tools -- bash
$ ceph status
  cluster:
    id:     967d0929-d6f8-4c24-bf02-8ee6665cb9d1
    health: HEALTH_OK
#+end_src

[[https://rook.io/docs/rook/latest/Troubleshooting/ceph-toolbox/][More info]]

*** Dashboard
Install choose

#+begin_src shell
kubectl port-forward svc/rook-ceph-mgr-dashboard 7000:7000 -n rook-ceph &
kubectl -n rook-ceph get secret rook-ceph-dashboard-password -o yaml |sed '3q;d' |choose 1 |base64 -d |wl-copy
#+end_src

Open <http://localhost:7000>

*** Teardown
Destroy fablab-management except persistent volumes, and the namespace
#+begin_src shell
kapp delete -a fablab-management -n default --filter '{"not":{"resource":{"kinds":["PersistentVolumeClaim","Namespace"]}}}'
#+end_src

To tear down rook-ceph, recreate the (minikube) cluster.

If you really want to delete a live cluster, run the following command and delete the CephCluster.

#+begin_src shell
kubectl -n rook-ceph patch cephcluster my-cluster --type merge -p '{"spec":{"cleanupPolicy":{"confirmation":"yes-really-destroy-data"}}}'
#+end_src

** Mainsail
To build the mainsail image, run

#+begin_src sh
  rm -f result.tar.gz; nix build .#mainsailContainer && cp result result.tar.gz
#+end_src

To start the mainsail server, run

#+begin_src sh
  podman run --rm -p 80:80 -it -v ./var:/var:z -v $PWD/upstreams.conf.example:/etc/nginx/upstreams.conf:z registry.gitlab.com/otafablab/fablab-nix/mainsail:latest
#+end_src

** Rook
*** Notes
While trying to deploy Rook to the fablab cluster the initial deployment with
one replicated pool went smoothly. However creating a ceph filesystem got stuck
in ~osd pool create myfs-metadata~. I tried adding SSDs and upgrading the
networking but nothing helped. When I got frustrated enough I deleted the whole
namespace along with the cluster but that didn't solve anything. New clusters
would fail to create the .mrg pool!

After trying to deploy to minikube, which worked flawlessly with the newest
configuration, I realized that the issue might be with some incompatible state
being leftover in other parts of the system than ~/var/lib/rook~ etc. So next I
tried deleting all k3s related data and bootstrapping the kubernetes cluster
again:

#+begin_src shell
pssh -PH "babbage carmack hellman iverson" "systemctl stop k3s && systemctl stop containerd && rm -rf /var/lib/{containerd,rancher,kubelet,cni,rook}"
#+end_src

Some nodes have orphans stuck and it failed to remove kubelet directory. Also
for some reason there is a ~ceph-msgr~ process running? Not sure what is the
point of it. After recreating the cluster, the following labels were set:

#+begin_src text
NAME      STATUS   ROLES                                          AGE     VERSION
amdahl    Ready    control-plane,etcd,master,rook-ceph-mon-node   13m     v1.27.7+k3s2
carmack   Ready    control-plane,etcd,master,rook-ceph-mon-node   13m     v1.27.7+k3s2
edelman   Ready    rook-ceph-storage-node                         7m35s   v1.27.7+k3s2
fredkin   Ready    rook-ceph-storage-node                         7m34s   v1.27.7+k3s2
gabriel   Ready    rook-ceph-storage-node                         7m32s   v1.27.7+k3s2
hellman   Ready    rook-ceph-mon-node                             13m     v1.27.7+k3s2
#+end_src

Now the only thing left was to deploy Rook.

**** Mounts
Example of a pod with a mount to 1TB volume visible on the node:

#+begin_src shell
λ df -h
/dev/rbd0              1007G   31G  977G   3% /var/lib/kubelet/pods/b2f1723a-e71b-4218-b5d1-eae1df945601/volumes/kubernetes.io~csi/pvc-18f6b659-032a-4d14-92b1-3857e8bf7c1e/mount
#+end_src

Also visible in lsblk:

#+begin_src shell
λ lsblk -f
rbd0                                                                         976.8G     3% /var/lib/kubelet/pods/b2f1723a-e71b-4218-b5d1-eae1df945601/volumes/kubernetes.io~csi/pvc-18f6b659-032a-4d14-92b1-3857e8bf7c1e/mount
#+end_src
**** Deleting OSDs
The job in ~./util/osd-purge.yaml~ does not delete the auth tokens for the OSDs which causes an error in the operator:

#+begin_src text
stderr: Error EEXIST: entity osd.8 exists but key does not match
#+end_src

To fix this, run in toolbox

#+begin_src shell
ceph auth del osd.8
#+end_src

The OSD pods need also deleting. Also the new disks get the bluestore filesystem so those have to be removed...

Deleting the provider pod restarts the OSD preparation jobs.

TODO it seems like the prepare jobs also fail to this auth token issue.
**** Monitor performance
The following error (appearing with \approx 1-2s intervals) seems to be caused by a lack of resources, CPU or network.

#+begin_src text
cephx server client.admin: handle_request failed to decode CephXAuthenticate: End of buffer
#+end_src

It is sometimes accompanied by ~mon.c@2(probing) e3 handle_auth_request failed to assign global_id~

Another similar issue <https://github.com/rook/rook/issues/11603>

Monitors going into 100% CPU is probably a bug or an issue with the cluster.
**** Monitors re-electing due to slow operations
<https://serverfault.com/questions/1036123/ceph-e5-handle-auth-request-failed-to-assign-global-id-after-a-host-outage>

Fix: increase mon_lease

Doesn't actually fix the issue
**** Migrating monitors
Just delete the mon with toolbox ~ceph mon rm c~ and the operator will reconcile shortly.

#+begin_src text
op-mon: starting new mon: &{ResourceName:rook-ceph-mon-f DaemonName:f PublicIP: Port:6789 Zone: NodeName: DataPathMap:0xc002081530 UseHostNetwork:false}
op-mon: scaling the mon "c" deployment to replica 0
#+end_src
**** BUG lsblk infinite loop
~lsblk -O /dev/sd{a,b}~
*** BUG kubectl rook-ceph
destroy cluster does not remove previous cleanup jobs and therefore doesn't clean up on successive attempts
*** External cluster
**** Deploy the rook-ceph-ext kubenix module

**** Start ceph container in containerd
Start a ceph container using ~nerdctl~. Requires access to /etc/ceph and host networking. Change the ceph version to match the one used by nixpkgs.

#+begin_src shell
nix run nixpkgs#nerdctl -- run -it --privileged --rm -v /nix:/nix -v /etc/static:/etc/static -v /etc/ceph:/etc/ceph --net host quay.io/ceph/ceph:v18.2 bash
ceph -s # check that ceph connection works
#+end_src

**** Create auth tokens and export environment
#+begin_src shell
curl https://raw.githubusercontent.com/rook/rook/release-1.13/deploy/examples/create-external-cluster-resources.py -O
python3 create-external-cluster-resources.py --rbd-data-pool-name test --namespace rook-ceph --format bash
#+end_src

Run ~ceph mgr module enable prometheus~ if it complains about it

To provision keys for the cephfs csi use the following command

#+begin_src shell
python3 create-external-cluster-resources.py --rbd-data-pool-name test --cephfs-filesystem-name cephfs --namespace rook-ceph --format bash
#+end_src

**** Importing to the cluster
On a host with access to the kubernetes cluster via kubectl, run the exports and
~util/import-external-cluster-dry.sh~. It will create various secrets, but also
creates the storage classes. The storage classes can be deleted as the kubenix
manifests already provide them.

The experience of these export/import is quite bad so at some point I might
write some simple commands to replace them.

**** Check operator logs
#+begin_src text
2024-01-09 17:09:58.696953 I | ceph-spec: parsing mon endpoints: edelman=10.42.0.15:6789
2024-01-09 17:09:58.697216 I | ceph-spec: updating obsolete maxMonID 2 to actual value 1594609003
2024-01-09 17:09:58.698798 I | op-k8sutil: ROOK_OBC_WATCH_OPERATOR_NAMESPACE="true" (configmap)
2024-01-09 17:09:58.698810 I | op-bucket-prov: ceph bucket provisioner launched watching for provisioner "rook-ceph.ceph.rook.io/bucket"
2024-01-09 17:09:58.706382 I | op-bucket-prov: successfully reconciled bucket provisioner
I0109 17:09:58.706824       1 manager.go:135] "msg"="starting provisioner" "logger"="objectbucket.io/provisioner-manager" "name"="rook-ceph.ceph.rook.io/bucket"
2024-01-09 17:10:44.060563 I | ceph-spec: parsing mon endpoints: edelman=10.42.0.15:6789
2024-01-09 17:10:44.060577 I | ceph-spec: updating obsolete maxMonID 2 to actual value 1594609003
2024-01-09 17:10:44.060623 I | ceph-spec: found the cluster info to connect to the external cluster. will use "client.healthchecker" to check health and monitor status. mons=map[edelman:0xc
001903920]
2024-01-09 17:10:44.066754 I | ceph-spec: detecting the ceph image version for image quay.io/ceph/ceph:v17.2.6...
2024-01-09 17:15:01.756497 I | ceph-spec: detected ceph image version: "17.2.6-0 quincy"
2024-01-09 17:15:01.756525 I | ceph-cluster-controller: validating ceph version from provided image
2024-01-09 17:15:01.761374 I | ceph-spec: parsing mon endpoints: edelman=10.42.0.15:6789
2024-01-09 17:15:01.761385 I | ceph-spec: updating obsolete maxMonID 2 to actual value 1594609003
2024-01-09 17:15:01.764533 I | cephclient: writing config file /var/lib/rook/rook-ceph/rook-ceph.config
2024-01-09 17:15:01.764583 I | cephclient: generated admin config in /var/lib/rook/rook-ceph
2024-01-09 18:05:16.853010 E | ceph-cluster-controller: failed to reconcile CephCluster "rook-ceph/ceph". failed to reconcile cluster "ceph": failed to configure external ceph cluster: failed to detect and validate ceph version: failed to validate ceph version between external and local: failed to get ceph mon version: failed to run 'ceph version'. . timed out: exit status 1
#+end_src

At this point I noticed that the configuration created by rook is invalid, as it
tries to connect to msgr v2 on port 3300 first which is not available on the
NixOS nodes running ceph.

File contents of ~/var/lib/rook/rook-ceph/rook-ceph.config~ on the rook operator

#+begin_src text
[global]
fsid                = 7ca0d35d-c5ac-4620-8d1a-9284f2adcd09
mon initial members = edelman
mon host            = [v2:10.42.0.15:3300,v1:10.42.0.15:6789]
#+end_src

Enabling messenger v2 on the monitor solved the issue.

**** TODO change to ~rook-ceph-mon~ secret
I changed ~cluster-name~ to ~ceph~ rather than ~rook-ceph~ but am not sure if it made a difference.

*** Kubelet
**** Unmounting broken mount points
Sometimes e.g. when force deleting pods, ceph-csi RBD mounts might be left
unmounted and succssive pods can't mount the PVC data. The problem is indicated
by a pod stuck at ~ContainerCreating~ and the following event

#+begin_src text
pronter-ctrl   79s         Warning   FailedMount              pod/moonraker-um2-0         MountVolume.SetUp failed for volume "pvc-a9a56be0-645e-443c-9741-d82a9b92e43b" : volume uses the ReadWriteOncePod access mode and is already in use by another pod
#+end_src

To fix the issue, log into the node and unmount the volume manually. To see the
mounts try ~mount |grep ceph~ and figure out which mount belongs to which
container.

(Rebooting the node might also be required in between)

However now kubelet is complaining about the following in k3s logs:

#+begin_src text
Jan 11 00:10:48 hellman k3s[11155]: E0111 00:10:48.125233   11155 nestedpendingoperations.go:348] Operation for "{volumeName:kubernetes.io/csi/rook-ceph.cephfs.csi.ceph.com^0001-0009-rook-ceph-0000000000000003-38f420d9-aab7-4a76-a110-86fcf8bdeac9 podName: nodeName:}" failed. No retries permitted until 2024-01-11 00:11:52.125215567 +0200 EET m=+133.073001016 (durationBeforeRetry 1m4s). Error: MountVolume.SetUp failed for volume "pvc-36ff8024-767a-46d0-a7dc-db07cb3eead4" (UniqueName: "kubernetes.io/csi/rook-ceph.cephfs.csi.ceph.com^0001-0009-rook-ceph-0000000000000003-38f420d9-aab7-4a76-a110-86fcf8bdeac9") pod "moonraker-um1-0" (UID: "df2ae1bd-90db-44b0-884d-a3d880308221") : rpc error: code = Internal desc = staging path /var/lib/kubelet/plugins/kubernetes.io/csi/rook-ceph.cephfs.csi.ceph.com/9f83fecacce878b627de915cc957c38fa8f383b01db252af679850cf9ca1b4fe/globalmount for volume 0001-0009-rook-ceph-0000000000000003-38f420d9-aab7-4a76-a110-86fcf8bdeac9 is not a mountpoint
Jan 11 00:11:44 hellman k3s[11155]: E0111 00:11:44.612082   11155 kubelet.go:1875] "Unable to attach or mount volumes for pod; skipping pod" err="unmounted volumes=[gcodes], unattached volumes=[], failed to process volumes=[]: timed out waiting for the condition" pod="pronter-ctrl/moonraker-um1-0"
Jan 11 00:11:44 hellman k3s[11155]: E0111 00:11:44.643548   11155 pod_workers.go:1294] "Error syncing pod, skipping" err="unmounted volumes=[gcodes], unattached volumes=[], failed to process volumes=[]: timed out waiting for the condition" pod="pronter-ctrl/moonraker-um2-0" podUID=3040e955-9a17-4238-b96d-1784a93df872
Jan 11 00:11:52 hellman k3s[11155]: I0111 00:11:52.172299   11155 operation_generator.go:623] "MountVolume.WaitForAttach entering for volume \"pvc-36ff8024-767a-46d0-a7dc-db07cb3eead4\" (UniqueName: \"kubernetes.io/csi/rook-ceph.cephfs.csi.ceph.com^0001-0009-rook-ceph-0000000000000003-38f420d9-aab7-4a76-a110-86fcf8bdeac9\") pod \"moonraker-um2-0\" (UID: \"3040e955-9a17-4238-b96d-1784a93df872\") DevicePath \"\"" pod="pronter-ctrl/moonraker-um2-0"
#+end_src

This can hackily be solved by mounting a tempfs at the mountpoint. However the
resource should be redeployed afterwards to get the persistent storage mounted
instead.

#+begin_src shell
mount -t tmpfs -o size=1M tmpfs /var/lib/kubelet/plugins/kubernetes.io/csi/rook-ceph.cephfs.csi.ceph.com/9f83fecacce878b627de915cc957c38fa8f383b01db252af679850cf9ca1b4fe/globalmount
#+end_src
**** Pods stuck terminating
Try deleting the task on the node the pod runs on. For example containers running the image ~klipper:~

#+begin_src shell
ctr -n k8s.io c ls |grep klipper: |cut -d ' ' -f 1 |xargs -n1 ctr -n k8s.io task kill -s 9
#+end_src

*** Oauth2-proxy
**** Redirect url
- redirect_url = "http://${host}/oauth2/callback"

**** OIDC issuer url
Oauth2 proxy uses the issuer URL to access the issuer on the backend side (in
the regular login flow) therefore it might make sense to supply an internal
domain name which can be resolved within the cluster.

#+begin_src nix
oidc_issuer_url = "http://keycloak.fablab-management/auth/realms/fablab"
#+end_src

However this has to be the same as what the client user logging in sees,
otherwise we get the following error

#+begin_src text
oauth2-proxy [2024/01/11 23:11:04] [oauthproxy.go:834] Error redeeming code during OAuth2 callback: could not verify id_token: failed to verify token: oidc: id token issued by a different provider, expected "http://keycloak/auth/realms/fablab" got "http://fablab.otaniemi.org/auth/realms/fablab"
#+end_src

Thus it needs to be a domain name not internal to the cluster.

Because keycloak needs to be configured separately for HTTP as for HTTPS I
decided to stop trying to make plain http work.

**** DNS hack
We can work around the limitation that the hostname needs to resolve to an IP
that routes back to the cluster using a custom dns entry in coredns. I think
this can be set up to support wildcard prefix also.

#+begin_src text
rewrite name fablab.otaniemi.org ingress-nginx-controller.ingress-nginx.svc.cluster.local
kubernetes cluster.local in-addr.arpa ip6.arpa {
[..]
#+end_src

After restarting the dns server, we can test whether the rewrite works

#+begin_src shell
kubectl exec -it -n fablab-management deploy/oauth2-proxy -- sh
Defaulted container "oauth2-proxy" out of: oauth2-proxy, wait-for-redis (init)
~ $ nslookup fablab.otaniemi.org
Server:         10.43.0.10
Address:        10.43.0.10:53


Name:   fablab.otaniemi.org
Address: 10.43.48.217
#+end_src

And it seems to work as it should, i.e. all internal services can access the
ingress controller using the ~fablab.otaniemi.org~ domain.

This hack is not needed if the network reroutes traffic headed to
~fablab.otaniemi.org~ through a private VPN where the firewall is open and
clients can access the reverse proxy running on the server.

**** Caching 302 => 502
If you see a 502 message on page
~/auth/realms/fablab/protocol/openid-connect/auth~ but the ingress nginx only
reports 302, it is likely that your browser is incorrectly caching the redirect.
Try restarting your browser.

Probably also need to clear cookies. Note that they have to be cleared after the
502 page also.

<https://github.com/oauth2-proxy/oauth2-proxy/issues/676>

After some investigation, it seems odd that attempting to access an authorized
page with (any) cookies present triggers this.

Maybe this is relevant? <https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview/#configuring-for-use-with-the-nginx-auth_request-directive>

Deleting the ~KEYCLOAK_IDENTITY~ and ~KEYCLOAK_IDENTITY_LEGACY~ cookies is
enough to get the login to show up instead of 502.

Facepalm: the issue was with bastion's nginx: ~upstream sent too big header while reading response header from upstream~

**** TLS
***** Ingress
A working ingress (and application) serving self-signed TLS traffic from ~test.fablab.otaniemi.org~ is available in ~./util/auth-test~. They ingress specification is as follows:

#+begin_src yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: auth-test
  annotations:
    nginx.ingress.kubernetes.io/auth-url: "https://fablab.otaniemi.org/oauth2/auth"
    nginx.ingress.kubernetes.io/auth-signin: "https://fablab.otaniemi.org/oauth2/start?rd=$scheme://$host$escaped_request_uri"
spec:
  ingressClassName: nginx
  rules:
  - host: test.fablab.otaniemi.org
    http:
      paths:
      - backend:
          service:
            name: auth-test
            port:
              number: 80
        path: /
        pathType: Prefix
  tls:
  - hosts:
    - test.fablab.otaniemi.org
#+end_src

Notice that auth-signin url is different from what is used in the tutorial
<https://kubernetes.github.io/ingress-nginx/examples/auth/oauth-external-auth/>.
This is because we need the redirect URL (specified after ~?rd~) to take
subdomains into account which the tutorial does not.

***** TODO downstream TLS settings (nixos)

**** Nginx ingress settings
- nginx.ingress.kubernetes.io/auth-url: "http://oauth2-proxy.fablab-management.svc.cluster.local/oauth2/auth"
- nginx.ingress.kubernetes.io/auth-signin: "http://fablab.otaniemi.org/oauth2/start?rd=$escaped_request_uri"

** Plan and design
*** Overview
The plan is to create Nix-based infrastructure for managing Ultimakers etc. remotely.

*** Components
- One Raspberry Pi running klipper and moonraker. Refer to ~/rpi~ directory.
  - The 3D printers are accessible through this raspberry pi.
- A kube cluster for database, logging, SSO and mainsail.
- Keycloak running on kubernetes. It will act as the SSO and identity provider. It will be configured to allow registration using eduespoo google oauth or email.
- A public bastion running a public ingress through which users connect.

*** Networking

*** Mainsail
*** Keycloak

** References
*** <https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/oauth_provider/#keycloak-oidc-auth-provider>
Keycloak configuration for oauth2-proxy.
*** <https://kubernetes.github.io/ingress-nginx/examples/auth/oauth-external-auth/>
Ingress-nginx integration with oauth2. Important takeaways are the annotations: ~nginx.ingress.kubernetes.io/auth-{url,signin}~
*** <https://kubenix.org/examples/deployment/>
Kubenix example
*** <https://www.youtube.com/watch?v=996OiexHze0>
Excellent introduction to oauth2 and oidc
