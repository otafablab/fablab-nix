{...}: {
  imports = [
    ./control-plane-node.nix
  ];

  nixpkgs.hostPlatform = "x86_64-linux";

  networking.hostName = "kube-chad";
  users.users.root.password = "";
  microvm = {
    mem = 8 * 1024;
    interfaces = [
      {
        type = "user";
        id = "qemu";
        mac = "02:00:00:01:01:01";
      }
    ];
    volumes = [
      {
        mountPoint = "/var";
        image = "var.img";
        size = 16 * 1024;
      }
    ];
    shares = [
      {
        # use "virtiofs" for MicroVMs that are started by systemd
        proto = "9p";
        tag = "ro-store";
        # a host's /nix/store will be picked up so that no
        # squashfs/erofs will be built for it.
        source = "/nix/store";
        mountPoint = "/nix/.ro-store";
      }
    ];

    hypervisor = "qemu";
    socket = "control.socket";
  };

  system.stateVersion = "23.05";
}
