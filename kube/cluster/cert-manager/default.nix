{
  config,
  kubenix,
  lib,
  name,
  args,
  ...
}: {
  imports = with kubenix.modules; [submodule k8s helm];

  # Args are used to pass information from the parent context.
  options.submodule.args = {
    kubernetes = lib.mkOption {
      description = "Kubernetes config to be applied to a specific namespace.";
      # We are not given a precise type to this since we are using it
      # to set kubernetes options from the k8s module which are already
      # precisely typed.
      type = lib.types.attrs;
      default = {};
    };
  };

  config = {
    submodule.name = "cert-manager";
    # Passthru allows a submodule instance to set config of the parent
    # context. It's not strictly required but it's useful for combining
    # outputs of multiple submodule instances, without having to write
    # ad hoc code in the parent context.

    # Here we set kubernetes.objects.
    # This is a list so even if distinct instances of the submodule contain
    # definitions of identical api resources, these will not be merged or
    # cause conflicts. Lists of resources from multiple submodule instances
    # will simply be concatenated.
    submodule.passthru.kubernetes.objects = config.kubernetes.objects;

    kubernetes = lib.mkMerge [
      {namespace = name;}
      {resources.namespaces.${name} = {};}
      {
        customTypes =
          map (pair: {
            kind = builtins.elemAt pair 0;
            group = "cert-manager.io";
            version = "v1";
            attrName = builtins.elemAt pair 1;
            module.options = with lib; lib.mkOption (types.attrsOf types.anything);
          }) [
            ["Issuer" "issuers"]
            ["ClusterIssuer" "clusterIssuers"]
          ];
        # TODO auto generate from helm crds
      }
      {
        helm.releases = {
          prometheus = {
            chart = kubenix.lib.helm.fetch {
              repo = "https://charts.jetstack.io";
              chart = "cert-manager";
              version = "v1.13.3";
              sha256 = "sha256-xVHJRW6ZwxdyF5b9SyssPQikObaccz1CVciqT6ACfok=";
            };
            values = {
              installCRDs = true;
              replicaCount = 3;
              podDisruptionBudget.enabled = true;
              webhook = {
                replicaCount = 3;
                podDisruptionBudget.enabled = true;
              };
              cainjector = {
                replicaCount = 3;
                podDisruptionBudget.enabled = true;
              };
              namespace = name;
              leaderElection.namespace = name; # https://github.com/cert-manager/cert-manager/issues/5471
              prometheus.enabled = true; # TODO how to disable prometheus?
            };
            # error: The option `submodules.instances.cert-manager.config.kubernetes.api.resources."rbac.authorization.k8s.io".v1.Role."prometheus-cert-manager-cainjector:leaderelection".metadata.namespace' has conflicting definition values:
            # namespace = name;
          };
        };
      }
      {
        resources = {
          secrets.cloudflare.stringData = {
            api-token = "ref+sops://../secrets/cert-manager.yaml?key_type=filepath&format=yaml#/cloudflare-dns/api-token";
          };
          clusterIssuers.letsencrypt-staging.spec.acme = {
            email = "niklas.halonen@protonmail.com";
            server = "https://acme-staging-v02.api.letsencrypt.org/directory";
            privateKeySecretRef.name = "letsencrypt-staging-account-key";
            solvers = [
              {
                dns01.cloudflare.apiTokenSecretRef = {
                  name = "cloudflare";
                  key = "api-token";
                };
                selector.dnsZones = ["fablab-systems.fi" "*.fablab-systems.fi"];
              }
            ];
          };
          clusterIssuers.letsencrypt.spec.acme = {
            email = "niklas.halonen@protonmail.com";
            server = "https://acme-v02.api.letsencrypt.org/directory";
            privateKeySecretRef.name = "letsencrypt-account-key";
            solvers = [
              {
                dns01.cloudflare.apiTokenSecretRef = {
                  name = "cloudflare";
                  key = "api-token";
                };
                selector.dnsZones = ["fablab-systems.fi" "*.fablab-systems.fi"];
              }
            ];
          };
        };
      }
      args.kubernetes
    ];
  };
}
