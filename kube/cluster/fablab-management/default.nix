{
  config,
  kubenix,
  lib,
  name,
  args,
  ...
}: let
  # FIXME Why can't these be in the parameters?
  inherit
    (args)
    host
    management-domain
    ;
in {
  imports = with kubenix.modules; [submodule k8s helm];

  # Args are used to pass information from the parent context.
  options.submodule.args = {
    kubernetes = lib.mkOption {
      description = "Kubernetes config to be applied to a specific namespace.";
      # We are not given a precise type to this since we are using it
      # to set kubernetes options from the k8s module which are already
      # precisely typed.
      type = lib.types.attrs;
      default = {};
    };

    host = lib.mkOption {
      description = "The domain and its subdomains that are managed.";
      type = lib.types.str;
    };

    management-domain = lib.mkOption {
      description = "The (sub)domain used only for keycloak and oauth2-proxy.";
      type = lib.types.str;
    };
  };

  config = {
    submodule.name = "fablab-management";
    submodule.passthru.kubernetes.objects = config.kubernetes.objects;

    kubernetes = lib.mkMerge [
      {namespace = name;}
      {resources.namespaces.${name} = {};}

      {
        helm.releases = {
          keycloak = {
            chart = kubenix.lib.helm.fetch {
              repo = "https://charts.bitnami.com/bitnami";
              chart = "keycloak";
              version = "17.3.4";
              sha256 = "sha256-t0tgaLx/63DZElleuBXYY6mOJESN/bVxq7t+ATOrDsY=";
            };

            # https://github.com/bitnami/charts/tree/main/bitnami/keycloak/
            values = {
              # global.storageClass = "ceph-rbd-ec"; # TODO ceph

              auth = {
                existingSecret = "keycloak";
                adminUser = "admin";
              };
              production = true;
              proxy = "edge";
              httpRelativePath = "/auth/";
              replicaCount = 3;
              # resources.requests # TODO

              ingress = {
                enabled = true;
                hostname = management-domain;
                path = "/auth";
                pathType = "Prefix";
                tls = true;
                ingressClassName = "nginx";
                annotations."nginx.ingress.kubernetes.io/proxy-buffer-size" = "512k";
                annotations."cert-manager.io/cluster-issuer" = "letsencrypt";
              };

              serviceAccount.create = false; # Not sure why it is enabled by default

              pdb.create = true;

              metrics.enabled = true;
              metrics.prometheusRule.enabled = true;
              # namespace?

              keycloakConfigCli = {
                enabled = true;
                extraEnvVars = [
                  {
                    name = "IMPORT_VARSUBSTITUTION_ENABLED";
                    value = "1";
                  }
                ];
                extraEnvVarsSecret = "keycloak-config";
                configuration.fablab = import ./fablab-realm.nix {inherit host management-domain;};
                cleanupAfterFinished.enabled = true; # Clean up after 10min
              };

              postgresql.auth.existingSecret = "keycloak-postgresql";
              # TODO postgresql replication
            };
            namespace = name;
          };

          oauth2-proxy = {
            chart = kubenix.lib.helm.fetch {
              repo = "https://oauth2-proxy.github.io/manifests";
              chart = "oauth2-proxy";
              version = "6.20.0";
              sha256 = "sha256-RinRkcjRra/32HhX9dKp3fcAqIu9kTE1bY8jt1XcnRM=";
            };

            values = {
              config = {
                # FIXME check settings before opening to public
                configFile = ''
                  provider = "keycloak-oidc"
                  provider_display_name = "Fablab Keycloak OIDC"
                  redirect_url = "https://${management-domain}/oauth2/callback"
                  oidc_issuer_url = "https://${management-domain}/auth/realms/fablab"
                  redis_connection_idle_timeout = 120
                  cookie_secure = true
                  cookie_domains = ["${host}"]
                  whitelist_domains = ["${host}", "*.${host}:*"]
                  email_domains = ["*"]
                  code_challenge_method = "S256"
                  insecure_oidc_allow_unverified_email = false
                  http_address = "0.0.0.0:4180"
                '';
                existingSecret = "oauth2-proxy";
              };
              redis = {
                enabled = true;
                # global.storageClass = "ceph-rbd-ec"; # TODO ceph
              };
              sessionStorage = {
                type = "redis";
              };
              ingress = {
                enabled = true;
                path = "/oauth2";
                pathType = "Prefix";
                hosts = [management-domain];
                tls = [
                  {
                    hosts = [management-domain];
                    secretName = "${management-domain}-tls"; # TODO is it ok to use the same secret for two ingresses
                  }
                ];
                className = "nginx";
                annotations."nginx.ingress.kubernetes.io/proxy-buffer-size" = "512k";
                annotations."cert-manager.io/cluster-issuer" = "letsencrypt";
              };
            };

            namespace = name;
          };
        };
      }
      {
        resources = {
          secrets = {
            keycloak-postgresql.stringData = {
              # NOTE path is relative to directory where vals is run
              password = "ref+sops://../secrets/fablab-management.yaml?key_type=filepath&format=yaml#/kube/keycloak-postgresql/password";
              postgres-password = "ref+sops://../secrets/fablab-management.yaml?key_type=filepath&format=yaml#/kube/keycloak-postgresql/postgres-password";
            };

            oauth2-proxy.metadata.labels.app = "oauth2-proxy";
            oauth2-proxy.stringData = {
              client-id = "oauth2-proxy";
              client-secret = "ref+sops://../secrets/fablab-management.yaml?key_type=filepath&format=yaml#/kube/oauth2-proxy/client-secret";
              cookie-secret = "ref+sops://../secrets/fablab-management.yaml?key_type=filepath&format=yaml#/kube/oauth2-proxy/cookie-secret";
            };

            keycloak.metadata.labels.app = "keycloak";
            keycloak.stringData = {
              admin-password = "ref+sops://../secrets/fablab-management.yaml?key_type=filepath&format=yaml#/kube/keycloak/admin-password";
            };
            keycloak-config.metadata.labels.app = "keycloak";
            keycloak-config.stringData.CLIENT_SECRET = config.kubernetes.resources.secrets.oauth2-proxy.stringData.client-secret;
          };
        };
      }
      args.kubernetes
    ];
  };
}
