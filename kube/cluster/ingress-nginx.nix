{
  config,
  kubenix,
  lib,
  name,
  args,
  ...
}: {
  imports = with kubenix.modules; [submodule k8s helm];

  # Args are used to pass information from the parent context.
  options.submodule.args = {
    kubernetes = lib.mkOption {
      description = "Kubernetes config to be applied to a specific namespace.";
      # We are not given a precise type to this since we are using it
      # to set kubernetes options from the k8s module which are already
      # precisely typed.
      type = lib.types.attrs;
      default = {};
    };
  };

  config = {
    submodule.name = "ingress-nginx";
    # Passthru allows a submodule instance to set config of the parent
    # context. It's not strictly required but it's useful for combining
    # outputs of multiple submodule instances, without having to write
    # ad hoc code in the parent context.

    # Here we set kubernetes.objects.
    # This is a list so even if distinct instances of the submodule contain
    # definitions of identical api resources, these will not be merged or
    # cause conflicts. Lists of resources from multiple submodule instances
    # will simply be concatenated.
    submodule.passthru.kubernetes.objects = config.kubernetes.objects;

    kubernetes = lib.mkMerge [
      # name is ignored, only one rook-ceph is currently supported
      {namespace = name;}
      {resources.namespaces.${name} = {};}
      {
        helm.releases = {
          ingress-nginx = {
            chart = kubenix.lib.helm.fetch {
              repo = "https://kubernetes.github.io/ingress-nginx";
              chart = "ingress-nginx";
              version = "4.9.0";
              sha256 = "sha256-lg4pZY0lh/ny9TTx2Ybjt+4ziU5RldMBiNZ7QTA1UBM=";
            };
            namespace = name;
            values = {
              controller = {
                allowSnippetAnnotations = true;
                config.proxy-buffer-size = "64k";
                config.client-header-buffer-size = "64k";
              };
            };
          };
        };
      }
      args.kubernetes
    ];
  };
}
