{
  config,
  kubenix,
  lib,
  name,
  args,
  ...
}: {
  imports = with kubenix.modules; [submodule k8s helm];

  # Args are used to pass information from the parent context.
  options.submodule.args = {
    kubernetes = lib.mkOption {
      description = "Kubernetes config to be applied to a specific namespace.";
      # We are not given a precise type to this since we are using it
      # to set kubernetes options from the k8s module which are already
      # precisely typed.
      type = lib.types.attrs;
      default = {};
    };
  };

  config = {
    submodule.name = "monitoring";
    # Passthru allows a submodule instance to set config of the parent
    # context. It's not strictly required but it's useful for combining
    # outputs of multiple submodule instances, without having to write
    # ad hoc code in the parent context.

    # Here we set kubernetes.objects.
    # This is a list so even if distinct instances of the submodule contain
    # definitions of identical api resources, these will not be merged or
    # cause conflicts. Lists of resources from multiple submodule instances
    # will simply be concatenated.
    submodule.passthru.kubernetes.objects = config.kubernetes.objects;

    kubernetes = lib.mkMerge [
      {namespace = name;}
      {resources.namespaces.${name} = {};}
      {
        helm.releases = {
          prometheus = {
            chart = kubenix.lib.helm.fetch {
              repo = "https://prometheus-community.github.io/helm-charts";
              chart = "prometheus";
              version = "25.8.2";
              sha256 = "sha256-gmfmMIUQMI6I52kpqKmF9Ul2d0MIT+4bcKRqPyHMDAc=";
            };
            # TODO circular dependency with rook-ceph. Can only be set after bootstrapping
            # values.server.persistentVolume.storageClass = "rook-ceph-block";
            # values.alertmanager.persistence.storageClass = "rook-ceph-block";
            # Don't schedule on unavailable nodes
            values.prometheus-node-exporter.tolerations = lib.mkForce [];
            namespace = name;
          };
          prometheus-operator-crds = {
            chart = kubenix.lib.helm.fetch {
              repo = "https://prometheus-community.github.io/helm-charts";
              chart = "prometheus-operator-crds";
              version = "8.0.1";
              sha256 = "sha256-7I+E0YVHin7UgJ8eDTQW2bSmrJS0UaXe9HKtwOV5CjM=";
            };
            namespace = name;
          };
          grafana = {
            chart = kubenix.lib.helm.fetch {
              repo = "https://grafana.github.io/helm-charts";
              chart = "grafana";
              version = "7.0.19";
              sha256 = "sha256-3dFdkScURbAXyJbRfOwDh2wPB/xqF540PXHY09OiAGI=";
            };
            values = {
              # replicas = 3; # doesn't work because they all try to mount the same storage???
              # ingress.enabled = true;
              # ingress.hosts = "grafana.fablab.otaniemi.org";
              testFramework.enabled = false;
              # TODO circular dependency with rook-ceph. Can only be set after bootstrapping
              # persistence = {
              #   enabled = true;
              #   storageClassName = "rook-ceph-block";
              # };
              # Required by having a PVC it seems
              # deploymentStrategy.type = "Recreate";
              dashboards.default.node-exporter-full = {
                gnetId = 1860; # https://grafana.com/grafana/dashboards/1860-node-exporter-full/
                revision = 33;
                datasource = "prometheus";
              };
              dashboards.default.ceph = {
                gnetId = 2842; # https://grafana.com/grafana/dashboards/2842-ceph-cluster/
                revision = 17;
                datasource = "prometheus";
              };
              dashboardProviders."dashboardproviders.yaml" = {
                apiVersion = 1;
                providers = [
                  {
                    name = "default";
                    orgId = 1;
                    folder = "";
                    type = "file";
                    disableDeletion = false;
                    editable = true;
                    options.path = "/var/lib/grafana/dashboards/default";
                  }
                ];
              };
              datasources."datasources.yaml" = {
                apiVersion = 1;
                datasources = [
                  {
                    name = "prometheus";
                    type = "prometheus";
                    url = "http://prometheus-server:80";
                    isDefault = true;
                    readonly = false;
                  }
                ];
              };
            };
            namespace = name;
          };
        };
        # Test for kubenix issue
        # resources.deployments.test.spec = {
        #   selector = {};
        #   template.spec.containers.test = {
        #     ports = [
        #       { containerPort = 53;
        #         protocol = "TCP";
        #       }
        #       { containerPort = 53;
        #         protocol = "UDP";
        #       }
        #     ];
        #   };
        # };
      }
      {
        # I wonder why there is no helm deployment for this
        # https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/main/bundle.yaml
        resources = {
          clusterRoleBindings.prometheus-operator = {
            metadata = {
              labels = {
                "app.kubernetes.io/component" = "controller";
                "app.kubernetes.io/name" = "prometheus-operator";
                "app.kubernetes.io/version" = "0.70.0";
              };
              name = "prometheus-operator";
            };
            roleRef = {
              apiGroup = "rbac.authorization.k8s.io";
              kind = "ClusterRole";
              name = "prometheus-operator";
            };
            subjects = [
              {
                kind = "ServiceAccount";
                name = "prometheus-operator";
                namespace = name; # NOTE used to be default
              }
            ];
          };
          clusterRoles.prometheus-operator = {
            metadata = {
              labels = {
                "app.kubernetes.io/component" = "controller";
                "app.kubernetes.io/name" = "prometheus-operator";
                "app.kubernetes.io/version" = "0.70.0";
              };
            };
            rules = [
              {
                apiGroups = ["monitoring.coreos.com"];
                resources = ["alertmanagers" "alertmanagers/finalizers" "alertmanagers/status" "alertmanagerconfigs" "prometheuses" "prometheuses/finalizers" "prometheuses/status" "prometheusagents" "prometheusagents/finalizers" "prometheusagents/status" "thanosrulers" "thanosrulers/finalizers" "thanosrulers/status" "scrapeconfigs" "servicemonitors" "podmonitors" "probes" "prometheusrules"];
                verbs = ["*"];
              }
              {
                apiGroups = ["apps"];
                resources = ["statefulsets"];
                verbs = ["*"];
              }
              {
                apiGroups = [""];
                resources = ["configmaps" "secrets"];
                verbs = ["*"];
              }
              {
                apiGroups = [""];
                resources = ["pods"];
                verbs = ["list" "delete"];
              }
              {
                apiGroups = [""];
                resources = ["services" "services/finalizers" "endpoints"];
                verbs = ["get" "create" "update" "delete"];
              }
              {
                apiGroups = [""];
                resources = ["nodes"];
                verbs = ["list" "watch"];
              }
              {
                apiGroups = [""];
                resources = ["namespaces"];
                verbs = ["get" "list" "watch"];
              }
              {
                apiGroups = ["networking.k8s.io"];
                resources = ["ingresses"];
                verbs = ["get" "list" "watch"];
              }
              {
                apiGroups = ["storage.k8s.io"];
                resources = ["storageclasses"];
                verbs = ["get"];
              }
            ];
          };

          deployments.prometheus-operator = {
            metadata = {
              labels = {
                "app.kubernetes.io/component" = "controller";
                "app.kubernetes.io/name" = "prometheus-operator";
                "app.kubernetes.io/version" = "0.70.0";
              };
            };
            spec = {
              replicas = 1;
              selector = {
                matchLabels = {
                  "app.kubernetes.io/component" = "controller";
                  "app.kubernetes.io/name" = "prometheus-operator";
                };
              };
              template = {
                metadata = {
                  annotations = {"kubectl.kubernetes.io/default-container" = "prometheus-operator";};
                  labels = {
                    "app.kubernetes.io/component" = "controller";
                    "app.kubernetes.io/name" = "prometheus-operator";
                    "app.kubernetes.io/version" = "0.70.0";
                  };
                };
                spec = {
                  automountServiceAccountToken = true;
                  containers = [
                    {
                      args = ["--kubelet-service=kube-system/kubelet" "--prometheus-config-reloader=quay.io/prometheus-operator/prometheus-config-reloader:v0.70.0"];
                      env = [
                        {
                          name = "GOGC";
                          value = "30";
                        }
                      ];
                      image = "quay.io/prometheus-operator/prometheus-operator:v0.70.0";
                      name = "prometheus-operator";
                      ports = [
                        {
                          containerPort = 8080;
                          name = "http";
                        }
                      ];
                      resources = {
                        limits = {
                          cpu = "200m";
                          memory = "200Mi";
                        };
                        requests = {
                          cpu = "100m";
                          memory = "100Mi";
                        };
                      };
                      securityContext = {
                        allowPrivilegeEscalation = false;
                        capabilities = {drop = ["ALL"];};
                        readOnlyRootFilesystem = true;
                      };
                    }
                  ];
                  nodeSelector = {"kubernetes.io/os" = "linux";};
                  securityContext = {
                    runAsNonRoot = true;
                    runAsUser = 65534;
                    seccompProfile = {type = "RuntimeDefault";};
                  };
                  serviceAccountName = "prometheus-operator";
                };
              };
            };
          };
          serviceAccounts.prometheus-operator = {
            automountServiceAccountToken = false;
            metadata.labels = {
              "app.kubernetes.io/component" = "controller";
              "app.kubernetes.io/name" = "prometheus-operator";
              "app.kubernetes.io/version" = "0.70.0";
            };
          };
          services.prometheus-operator = {
            metadata.labels = {
              "app.kubernetes.io/component" = "controller";
              "app.kubernetes.io/name" = "prometheus-operator";
              "app.kubernetes.io/version" = "0.70.0";
            };
            spec = {
              clusterIP = "None";
              ports = [
                {
                  name = "http";
                  port = 8080;
                  targetPort = "http";
                }
              ];
              selector = {
                "app.kubernetes.io/component" = "controller";
                "app.kubernetes.io/name" = "prometheus-operator";
              };
            };
          };
        };
      }
      args.kubernetes
    ];
  };
}
