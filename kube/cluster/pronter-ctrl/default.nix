{
  config,
  kubenix,
  lib,
  pkgs,
  name,
  args,
  ...
}: let
  inherit (args) host management-domain;
in {
  imports = with kubenix.modules; [submodule k8s helm];

  # Args are used to pass information from the parent context.
  options.submodule.args = {
    kubernetes = lib.mkOption {
      description = "Kubernetes config to be applied to a specific namespace.";
      # We are not given a precise type to this since we are using it
      # to set kubernetes options from the k8s module which are already
      # precisely typed.
      type = lib.types.attrs;
      default = {};
    };

    klipperImage = lib.mkOption {
      type = lib.types.str;
    };

    host = lib.mkOption {type = lib.types.str;};

    management-domain = lib.mkOption {type = lib.types.str;};
  };

  config = {
    submodule.name = "pronter-ctrl";
    # Passthru allows a submodule instance to set config of the parent
    # context. It's not strictly required but it's useful for combining
    # outputs of multiple submodule instances, without having to write
    # ad hoc code in the parent context.

    # Here we set kubernetes.objects.
    # This is a list so even if distinct instances of the submodule contain
    # definitions of identical api resources, these will not be merged or
    # cause conflicts. Lists of resources from multiple submodule instances
    # will simply be concatenated.
    submodule.passthru.kubernetes.objects = config.kubernetes.objects;

    kubernetes = let
      printers = {
        um1 = "/dev/serial/by-path/pci-0000:00:14.0-usb-0:7:1.0";
        um2 = "/dev/serial/by-path/pci-0000:00:14.0-usb-0:6:1.0";
      };
    in
      lib.mkMerge [
        {namespace = name;}
        {resources.namespaces.${name} = {};}
        {
          resources = {
            deployments.mainsail.spec = {
              replicas = 2;
              selector.matchLabels.app = "mainsail";
              template = {
                metadata.labels.app = "mainsail";
                spec.containers.mainsail.image = "ghcr.io/mainsail-crew/mainsail:latest"; # https://github.com/mainsail-crew/mainsail/pkgs/container/mainsail
              };
            };

            services.mainsail.spec = {
              type = "ClusterIP";
              selector.app = "mainsail";
              ports = [
                {
                  name = "http";
                  port = 80;
                }
              ];
            };

            configMaps.moonraker-config.data = let
              format = with lib;
                pkgs.formats.ini {
                  # https://github.com/NixOS/nixpkgs/pull/121613#issuecomment-885241996
                  listToValue = l:
                    if builtins.length l == 1
                    then generators.mkValueStringDefault {} (head l)
                    else lib.concatMapStrings (s: "\n  ${generators.mkValueStringDefault {} s}") l;
                  mkKeyValue = generators.mkKeyValueDefault {} ":";
                };
              formatINI = import ./formatINI.nix {inherit lib;};
            in {
              # builtins.readFile ./upstreams.conf.example;
              # FIXME IFD!
              "moonraker.conf" = builtins.readFile (format.generate "moonraker.conf" (lib.recursiveUpdate {
                server.klippy_uds_address = "/opt/printer_data/run/klipper.sock";
                server.port = 7125;
                machine.validate_service = false;
              } (import ./moonraker-base.nix)));
              # FIXME IFD!
              "printer.cfg" = builtins.readFile (builtins.toFile "klipper.cfg" (formatINI (lib.recursiveUpdate {
                mcu.serial = "/dev/ttyACM0";
                virtual_sdcard.path = "/opt/printer_data/gcodes";
              } (import ./ultimaker-2-plus.nix))));
            };
          };
        }
        {
          resources = {
            # TODO ceph is disabled
            # persistentVolumeClaims.moonraker-gcodes.spec = {
            #   accessModes = ["ReadWriteMany"];
            #   # CephFS supports mounting to multiple pods, RDB doesn't
            #   storageClassName = "cephfs";
            #   resources.requests.storage = "32Gi";
            # };

            # FIXME Response Error: [Errno 30] Read-only file system: '/opt/printer_data/config/gcodes-2024029-165610.zip' (files/downloadZip) when trying to download gcode
            statefulSets =
              lib.attrsets.concatMapAttrs (name: path: {
                "moonraker-${name}".spec = {
                  replicas = 1; # cannot replicate due to klipper owning the device
                  serviceName = "moonraker-${name}";
                  selector.matchLabels.app = "moonraker-${name}";
                  # volumeClaimTemplates = [
                  #   {
                  #     metadata.name = "database";
                  #     spec = {
                  #       accessModes = ["ReadWriteOncePod"];
                  #       # storageClassName = "ceph-rbd-ec"; # TODO ceph
                  #       resources.requests.storage = "1Gi"; # TODO is this enough?
                  #     };
                  #   }
                  #   {
                  #     # NOTE this is a temporary volume claim as ceph is dis
                  #     metadata.name = "gcodes";
                  #     spec = {
                  #       accessModes = ["ReadWriteOncePod"];
                  #       resources.requests.storage = "10Gi";
                  #     };
                  #   }
                  # ];
                  template = {
                    metadata.labels.app = "moonraker-${name}";
                    spec = {
                      # Ultimakers are connected to this node
                      # TODO should have affinity and prioritize 3D printing
                      nodeName = "hellman";
                      # FIXME
                      # terminationGracePeriodSeconds = 60 * 60 * 24 * 3; # Stop only if the print lasts for more 3 days
                      securityContext = {
                        runAsNonRoot = true;
                        runAsGroup = 1000;
                        runAsUser = 1000;
                        fsGroup = 1000;
                        supplementalGroups = [27]; # TODO is this always dialout on nixos?
                      };
                      containers.moonraker = {
                        image = "docker.io/mkuf/moonraker:latest";
                        volumeMounts = {
                          "/opt/printer_data/config".name = "config";
                          "/opt/printer_data/run".name = "run";
                          "/opt/printer_data/gcodes".name = "gcodes";
                          "/opt/printer_data/database".name = "database";
                        };
                        resources.requests = {
                          memory = "1Gi";
                          cpu = "1";
                        };
                        # https://moonraker.readthedocs.io/en/latest/web_api/#basic-print-status
                        # Only stop the print if the printer is in state standby, complete or error.
                        # If a PreStop hook fails, it kills the Container.
                        # FIXME
                        # lifecycle.preStop.exec.command = [
                        #   "sh"
                        #   "-c"
                        #   # TODO is there a way to shut down klipper after finishing
                        #   ''
                        #     while ! curl -s 'http://localhost:7125/printer/objects/query?print_stats' |grep -E '"state":\s+"(standby|complete|error)"'
                        #     do
                        #         sleep 30
                        #     done
                        #   ''
                        # ];
                      };
                      containers.klipper = {
                        # TODO fix klipper
                        # image = args.klipperImage;
                        image = "docker.io/mkuf/klipper:latest";
                        volumeMounts = {
                          "/dev/ttyACM0".name = "usb";
                          "/opt/printer_data/config".name = "config";
                          "/opt/printer_data/run".name = "run";
                          "/opt/printer_data/gcodes".name = "gcodes";
                        };
                        securityContext = {
                          privileged = true; # TODO is there some way to not set this
                        };
                        # FIXME klipper doesn't stop
                        # Stop klipper only after moonraker is stopped. Python will exit with connection refused when moonraker has stopped
                        # lifecycle.preStop.exec.command = [
                        #   "sh"
                        #   "-c"
                        #   # Sleep 1 second to observe moonraker exiting first
                        #   ''
                        #     sleep 1
                        #     while python -c "import socket; socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect(('127.0.0.1', 7125))"
                        #         do sleep 30
                        #     done
                        #     echo EXIT > /dev/pts/0
                        #   ''
                        # ];
                      };
                      volumes = {
                        usb.hostPath = {inherit path;};
                        config.configMap.name = "moonraker-config";
                        run.emptyDir = {};
                        gcodes.emptyDir = {};
                        database.emptyDir = {};
                        # gcodes.persistentVolumeClaim.claimName = "moonraker-gcodes"; # TODO ceph is disabled
                      };
                    };
                  };
                };
              })
              printers;

            services =
              lib.attrsets.concatMapAttrs (name: _: {
                "moonraker-${name}".spec = {
                  type = "ClusterIP";
                  selector.app = "moonraker-${name}";
                  ports = [
                    {
                      name = "moonraker";
                      port = 7125;
                    }
                  ];
                };
              })
              printers;

            ingresses.pronter-ctrl.metadata.annotations = {
              # https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/#custom-max-body-size
              "nginx.ingress.kubernetes.io/proxy-body-size" = "1000m";
              "nginx.ingress.kubernetes.io/proxy-buffer-size" = "128k";
              # "nginx.ingress.kubernetes.io/auth-url" = "https://${management-domain}/oauth2/auth";
              # "nginx.ingress.kubernetes.io/auth-signin" = "https://${management-domain}/oauth2/start?rd=$scheme://$host$escaped_request_uri";
            };
            ingresses.pronter-ctrl.spec = {
              ingressClassName = "nginx";
              tls = [{hosts = builtins.map (name: "${name}.${host}") (builtins.attrNames printers);}];
              rules = builtins.map (name: {
                host = "${name}.${host}";
                http.paths = [
                  {
                    path = "/websocket";
                    pathType = "Prefix";
                    backend.service = {
                      name = "moonraker-${name}";
                      port.name = "moonraker";
                    };
                  }
                  {
                    path = "/server";
                    pathType = "Prefix";
                    backend.service = {
                      name = "moonraker-${name}";
                      port.name = "moonraker";
                    };
                  }
                  {
                    # Everything else is always served by mainsail service
                    path = "/";
                    pathType = "Prefix";
                    backend.service = {
                      name = "mainsail";
                      port.name = "http";
                    };
                  }
                ];
              }) (builtins.attrNames printers);
            };
          };
        }
        args.kubernetes
      ];
  };
}
