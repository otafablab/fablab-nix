{
  dockerTools,
  klipper,
}: let
  uid = "1000";
in
  dockerTools.streamLayeredImage {
    name = "klipper";
    contents = [klipper];
    extraCommands = ''
      mkdir -p etc
      chmod u+w etc
      echo "klipper:x:${uid}:${uid}::/:" > etc/passwd
      echo "klipper:x:${uid}:klipper" > etc/group
      echo "tty:x:3:klipper" >> etc/group
      echo "dialout:x:27:klipper" >> etc/group
    '';
    # enableFakechroot = true; # This breaks ownership?
    fakeRootCommands = ''
      mkdir -p ./opt/printer_data/run ./opt/printer_data/gcodes ./opt/printer_data/logs ./opt/printer_data/config
      chown -R ${uid}:${uid} ./opt
    '';
    config = {
      User = uid;
      WorkingDir = "/opt";
      Entrypoint = ["${klipper.outPath}/bin/klippy"];
      Cmd = ["-I" "/opt/printer_data/run/klipper.tty" "-a" "/opt/printer_data/run/klipper.sock" "/opt/printer_data/config/printer.cfg"];
    };
  }
