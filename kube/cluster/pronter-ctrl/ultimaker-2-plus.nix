# generated using ini2nix.py
{
  "mcu" = {
    "restart_method" = "arduino";
  };
  "virtual_sdcard" = {
    "on_error_gcode" = "CANCEL_PRINT";
  };
  "pause_resume" = { };
  "display_status" = { };
  "display" = {
    "lcd_type" = "ssd1306";
    "reset_pin" = "PE3";
    "menu_timeout" = "60";
    "click_pin" = "^!PD2";
    "encoder_pins" = "^PG1, ^PG0";
    "encoder_steps_per_detent" = "2";
  };
  "pca9632 wheel_leds" = {
    "i2c_address" = "96";
    "initial_red" = "0.3";
    "initial_green" = "0.0";
    "initial_blue" = "1.0";
  };
  "exclude_object" = { };
  "respond" = {
    "default_type" = "echo";
  };
  "force_move" = {
    "enable_force_move" = "False";
  };
  "stepper_x" = {
    "step_pin" = "PA3";
    "dir_pin" = "!PA1";
    "enable_pin" = "!PA5";
    "microsteps" = "16";
    "rotation_distance" = "40";
    "endstop_pin" = "^!PA0";
    "position_endstop" = "0";
    "position_max" = "222";
    "homing_speed" = "60.0";
  };
  "stepper_y" = {
    "step_pin" = "PC5";
    "dir_pin" = "PC4";
    "enable_pin" = "!PC6";
    "microsteps" = "16";
    "rotation_distance" = "40";
    "endstop_pin" = "^!PA4";
    "position_endstop" = "218";
    "position_max" = "218";
    "homing_speed" = "60.0";
  };
  "stepper_z" = {
    "step_pin" = "PC2";
    "dir_pin" = "!PC1";
    "enable_pin" = "!PC3";
    "microsteps" = "16";
    "rotation_distance" = "16";
    "endstop_pin" = "^!PA7";
    "position_endstop" = "214";
    "position_max" = "214";
    "homing_speed" = "30.0";
    "second_homing_speed" = "3.0";
    "homing_retract_dist" = "3.0";
  };
  "extruder" = {
    "step_pin" = "PL7";
    "dir_pin" = "!PL6";
    "enable_pin" = "!PC0";
    "microsteps" = "16";
    "rotation_distance" = "33.500";
    "gear_ratio" = "36:11";
    "nozzle_diameter" = "0.400";
    "filament_diameter" = "2.850";
    "heater_pin" = "PE4";
    "sensor_type" = "PT100 INA826";
    "sensor_pin" = "PK0";
    "control" = "pid";
    "pid_kp" = "14.037";
    "pid_ki" = "0.755";
    "pid_kd" = "65.272";
    "min_temp" = "0";
    "max_temp" = "275";
    "max_extrude_only_distance" = "780.0";
    "min_extrude_temp" = "170";
    "max_extrude_cross_section" = "100";
    "pressure_advance" = "0.2";
    "pressure_advance_smooth_time" = "0.040";
    "max_extrude_only_velocity" = "60";
    "max_extrude_only_accel" = "500";
  };
  "firmware_retraction" = {
    "retract_length" = "5";
    "retract_speed" = "20";
    "unretract_extra_length" = "0";
    "unretract_speed" = "20";
  };
  "verify_heater extruder" = {
    "max_error" = "120";
    "check_gain_time" = "30";
  };
  "heater_fan heatbreak_cooling_fan" = {
    "pin" = "PJ6";
    "max_power" = "1";
    "shutdown_speed" = "0";
    "heater" = "extruder";
    "heater_temp" = "50.0";
    "fan_speed" = "0.6";
  };
  "heater_bed" = {
    "heater_pin" = "PG5";
    "sensor_type" = "PT100 INA826";
    "sensor_pin" = "PK2";
    "min_temp" = "0";
    "max_temp" = "110";
    "control" = "pid";
    "pid_kp" = "73.296";
    "pid_ki" = "1.623";
    "pid_kd" = "127.326";
  };
  "fan" = {
    "pin" = "PH4";
    "max_power" = "0.9";
    "kick_start_time" = "0.100";
    "off_below" = "0.09";
  };
  "output_pin beeper_pin" = {
    "pin" = "PD3";
    "pwm" = "True";
    "value" = "0";
    "shutdown_value" = "0";
    "cycle_time" = "0.001";
  };
  "printer" = {
    "kinematics" = "cartesian";
    "max_velocity" = "500";
    "max_accel" = "1500";
    "max_z_velocity" = "25";
    "max_z_accel" = "30";
  };
  "homing_override" = {
    "axes" = "xyz";
    "gcode" = ''
      G90
      G28 Z0
      G28 X0 Y0
    '';
  };
  "gcode_arcs" = {
    "resolution" = "1.0";
  };
  "led case_light" = {
    "white_pin" = "PH5";
    "cycle_time" = "0.010";
    "initial_white" = "0.3";
  };
  "output_pin stepper_xy_current" = {
    "pin" = "PL5";
    "pwm" = "True";
    "scale" = "2.000";
    "cycle_time" = ".000030";
    "hardware_pwm" = "True";
    "static_value" = "1.200";
  };
  "output_pin stepper_z_current" = {
    "pin" = "PL4";
    "pwm" = "True";
    "scale" = "2.000";
    "cycle_time" = ".000030";
    "hardware_pwm" = "True";
    "static_value" = "1.200";
  };
  "output_pin stepper_e_current" = {
    "pin" = "PL3";
    "pwm" = "True";
    "scale" = "2.000";
    "cycle_time" = ".000030";
    "hardware_pwm" = "True";
    "static_value" = "1.250";
  };
  "gcode_macro CANCEL_PRINT" = {
    "description" = "Cancel the actual running print";
    "rename_existing" = "CANCEL_PRINT_BASE";
    "variable_park" = "True";
    "gcode" = ''
      {% if printer.pause_resume.is_paused|lower == 'false' and park|lower == 'true' %}
      _TOOLHEAD_PARK_PAUSE_CANCEL
      {% endif %}
      TURN_OFF_HEATERS
      M106 S0
      CANCEL_PRINT_BASE
    '';
  };
  "gcode_macro PAUSE" = {
    "description" = "Pause the actual running print";
    "rename_existing" = "PAUSE_BASE";
    "gcode" = ''
      PAUSE_BASE
      _TOOLHEAD_PARK_PAUSE_CANCEL
    '';
  };
  "gcode_macro RESUME" = {
    "description" = "Resume the actual running print";
    "rename_existing" = "RESUME_BASE";
    "gcode" = ''
      {% set extrude = printer['gcode_macro _TOOLHEAD_PARK_PAUSE_CANCEL'].extrude %}
      {% if 'VELOCITY' in params|upper %}
      {% set get_params = ('VELOCITY=' + params.VELOCITY)  %}
      {%else %}
      {% set get_params = "" %}
      {% endif %}
      {% if printer.extruder.can_extrude|lower == 'true' %}
      M83
      G1 E{extrude} F2100
      {% if printer.gcode_move.absolute_extrude |lower == 'true' %} M82 {% endif %}
      {% else %}
      {action_respond_info("Extruder not hot enough")}
      {% endif %}
      RESUME_BASE {get_params}
    '';
  };
  "gcode_macro _TOOLHEAD_PARK_PAUSE_CANCEL" = {
    "description" = "Helper: park toolhead used in PAUSE and CANCEL_PRINT";
    "variable_extrude" = "1.0";
    "gcode" = ''
      {% set x_park = printer.toolhead.axis_maximum.x|float - 5.0 %}
      {% set y_park = printer.toolhead.axis_maximum.y|float - 5.0 %}
      {% set z_park_delta = 2.0 %}
      {% set max_z = printer.toolhead.axis_maximum.z|float %}
      {% set act_z = printer.toolhead.position.z|float %}
      {% if act_z < (max_z - z_park_delta) %}
      {% set z_safe = z_park_delta %}
      {% else %}
      {% set z_safe = max_z - act_z %}
      {% endif %}
      {% if printer.extruder.can_extrude|lower == 'true' %}
      M83
      G1 E-{extrude} F2100
      {% if printer.gcode_move.absolute_extrude |lower == 'true' %} M82 {% endif %}
      {% else %}
      {action_respond_info("Extruder not hot enough")}
      {% endif %}
      {% if "xyz" in printer.toolhead.homed_axes %}
      G91
      G1 Z{z_safe} F900
      G90
      G1 X{x_park} Y{y_park} F6000
      {% if printer.gcode_move.absolute_coordinates|lower == 'false' %} G91 {% endif %}
      {% else %}
      {action_respond_info("Printer not homed")}
      {% endif %}
    '';
  };
  "gcode_macro START_PRINT" = {
    "gcode" = ''
      G21 		; metric values
      G90 		; absolute positioning
      M82 		; set extruder to absolute mode
      M107 		; start with the fan off

      M220 S100 ;Reset feedrate
      M221 S100 ;Reset flowrate

      {% set bedtemp = params.BED|int %}
      {% set hotendtemp = params.HOTEND|int %}

      M140 S{bedtemp}                            ; set bed temp

      G28	; move X/Y/Z to endstops
      G1 Z6 F5000
      G1 X29 Y1 F5000

      M117 Bed...
      M190 S{bedtemp}                            ; wait for bed temp
      M117 Nozzle...
      M109 S{hotendtemp}                         ; set & wait for hotend temp

      M83 		   ; set extruder to relative mode
      G92 E0           ; reset extrusion distance
      G1 Z0.28
      G1 X39 E15 F500  ; fat line
      G1 X200 E5 F1500
      G1 Y1.6 F5000
      G1 X39 E5 F1500
      G1 E-0.5 F3000   ; retract

      G92 E0           ; reset extruder reference
      M82 		   ; set extruder to absolute mode
    '';
  };
  "gcode_macro PRINT_START_HT" = {
    "gcode" = ''
      G21 		; metric values
      G90 		; absolute positioning
      M82 		; set extruder to absolute mode
      M107 		; start with the fan off

      {% set bedtemp = params.BED|int %}
      {% set hotendtemp = params.HOTEND|int %}

      M190 S{bedtemp}                            ; set & wait for bed temp
      M117 Heating Bed...
      M109 S{hotendtemp}                         ; set & wait for hotend temp
      M117 Nozzle...

      SMART_HOME                                 ; final z homing
      G90                                        ; absolute positioning
      M83 		; set extruder to relative mode
      G92 E0           ; reset extrusion distance
      M117 Purging...
      G1 X217 Y188 Z2.6 F10000 ;move infront of bed clip
      G1 E20 F300 ;purge
      G1 X217 Y213 Z2.6 F10000 ;wipe nozzle on bed clip

      G92 E0           ; reset extruder reference
      M82 		; set extruder to absolute mode
    '';
  };
  "gcode_macro END_PRINT" = {
    "gcode" = ''
      G91 			; relative coordinates
      G1 E-6 X1 F3000	; retract the filament
      G28

      M104 S0
      M140 S0
      M106 S0
      M84
    '';
  };
  "gcode_macro print_end_warm" = {
    "gcode" = ''
      G91 			; relative coordinates
      G1 E-2 F3000		; retract the filament
      G1 Z+15  X-10 Y-10 E-6  F6000		; move Z a bit
      G1 X-10 Y-10 F6000	; move XY a bit
      G1 E-6 F300		; retract the filament
      M104 S100			; extruder heater off
      M140 S0			; heated bed heater off (if you have it)
    '';
  };
  "gcode_macro print_end_varioshore" = {
    "gcode" = ''
      G91 			; relative coordinates
      G1 E-1 F6000		; retract the filament
      G1 Z+20  X-10 Y-10 F6000		; move Z a bit
      G1 X-10 Y-10 F6000	; move XY a bit
      M104 S120			; extruder heater off
      M140 S0			; heated bed heater off (if you have it)
    '';
  };
  "gcode_macro UNLOAD_FILAMENT" = {
    "gcode" = ''
      M117 Heating-
      M104 S205	; start heating
      M109 S210 ; wait till hot
      M117 Unloading...
      M83                            ; set extruder to relative
      G1 X125 Y50 Z80                ; move to servicing position
      G1 E10 F300                    ; extrude a little to soften tip
      G1 E-10 F3000                  ; jerk the filament out of the melt zone
      G1 E-50 F1800                  ; retract the rest of the way
      G4 S4                          ;wait for cool
      G1 E-100 F8000                 ; retract the rest of the way
      G1 E-100
      G1 E-100
      G1 E-100 F10000
      G1 E-100
      G1 E-100
      G1 E-100 F5000
      G1 E-100 F3000
      M82
      M117 Sucess!
    '';
  };
  "gcode_macro LOAD_FILAMENT" = {
    "gcode" = ''
      M117 Heating+
      M104 S205	; start heating
      M109 S205 ; wait till hot
      M117 Unloading...
      M83           ; set extruder to relative
      G1 E20 F500   ; slow load
      G1 E100 F6000
      G1 E100
      G1 E100 F10000
      G1 E100
      G1 E100
      G1 E100
      G1 E100
      G1 E15 F150   ; prime nozzle with filament
      G1 E-5        ;retract
      M82
    '';
  };
  "idle_timeout" = {
    "gcode" = ''
      _RE_ENABLE_STEPPERS
      M117 Going Idle
    '';
    "timeout" = "600";
  };
  "gcode_macro _RE_ENABLE_STEPPERS" = {
    "description" = "enable steppr motors";
    "gcode" = "M84";
  };
  "gcode_macro SMART_HOME" = {
    "gcode" = ''
      {% if printer.toolhead.homed_axes == "xyz" %}
      M118 Printer is already homed
      {% else %}
      M118 Printer needs homed...
      G28
      {% endif %}
    '';
  };
  "gcode_macro M500" = {
    "gcode" = "SAVE_CONFIG";
  };
  "gcode_macro M300" = {
    "gcode" = ''
      {% set S = params.S|default(1000)|int %}
      {% set P = params.P|default(100)|int %}
      SET_PIN PIN=BEEPER_pin VALUE=0.5 CYCLE_TIME={ 1.0/S if S > 0 else 1 }
      G4 P{P}
      SET_PIN PIN=BEEPER_pin VALUE=0
    '';
  };
  "gcode_macro LEDON" = {
    "gcode" = ''
      SET_LED LED=case_light WHITE=0.25
      SET_LED LED=case_light WHITE=0.5
      SET_LED LED=case_light WHITE=0.75
      SET_LED LED=case_light WHITE=1.0
    '';
  };
  "gcode_macro LEDOFF" = {
    "gcode" = ''
      SET_LED LED=case_light WHITE=0.5
      SET_LED LED=case_light WHITE=0.0
    '';
  };
  "gcode_macro READY" = {
    "gcode" = ''
      SET_LED LED=case_light WHITE=.25
      SET_LED LED=case_light WHITE=.1
    '';
  };
  "gcode_macro XYSTRESSTEST" = {
    "gcode" = ''
      G1 X5 F30000
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 X5 ;
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 X5 ;
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 X5 ;
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 X5 ;
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 X5 F30000
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 X5 ;
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 X5 ;
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 X5 ;
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 X5 ;
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 X5 F30000
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 X5 ;
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 X5 ;
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 X5 ;
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 X5 ;
      G1 Y200
      G1 X200
      G1 Y5 ;
      G1 X5 Y200 ; go close to home
      G28 X0 Y0
    '';
  };
}
