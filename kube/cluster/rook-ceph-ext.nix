{
  config,
  kubenix,
  lib,
  name,
  args,
  ...
}: {
  imports = with kubenix.modules; [submodule k8s helm];

  # Args are used to pass information from the parent context.
  options.submodule.args = {
    kubernetes = lib.mkOption {
      description = "Kubernetes config to be applied to a specific namespace.";
      # We are not given a precise type to this since we are using it
      # to set kubernetes options from the k8s module which are already
      # precisely typed.
      type = lib.types.attrs;
      default = {};
    };
  };

  config = {
    submodule.name = "rook-ceph-ext";
    # Passthru allows a submodule instance to set config of the parent
    # context. It's not strictly required but it's useful for combining
    # outputs of multiple submodule instances, without having to write
    # ad hoc code in the parent context.

    # Here we set kubernetes.objects.
    # This is a list so even if distinct instances of the submodule contain
    # definitions of identical api resources, these will not be merged or
    # cause conflicts. Lists of resources from multiple submodule instances
    # will simply be concatenated.
    submodule.passthru.kubernetes.objects = config.kubernetes.objects;

    kubernetes = lib.mkMerge [
      # name is ignored, only one rook-ceph is currently supported
      {namespace = "rook-ceph";}
      {resources.namespaces.rook-ceph = {};}
      {
        customTypes =
          (map (pair: {
              kind = builtins.elemAt pair 0;
              group = "ceph.rook.io";
              version = "v1";
              attrName = builtins.elemAt pair 1;
              module.options = with lib; lib.mkOption (types.attrsOf types.anything);
            }) [
              ["CephFilesystem" "cephFilesystems"]
              ["CephObjectStore" "cephObjectStores"]
              ["CephBlockPool" "cephBlockPools"]
              ["CephCluster" "cephClusters"]
              ["CephFilesystem" "cephFilesystems"]
              ["CephFilesystemSubVolumeGroup" "cephFilesystemSubVolumeGroups"]
            ])
          ++ (map (pair: {
              kind = builtins.elemAt pair 0;
              group = "monitoring.coreos.com";
              version = "v1";
              attrName = builtins.elemAt pair 1;
              module.options = with lib; lib.mkOption (types.attrsOf types.anything);
            }) [
              ["PrometheusRule" "prometheusRules"]
              ["Prometheus" "prometheuses"]
              ["ServiceMonitor" "serviceMonitors"]
            ]);
        # TODO auto generate from helm crds
      }
      {
        helm.releases = {
          rook-ceph = {
            chart = kubenix.lib.helm.fetch {
              repo = "https://charts.rook.io/release";
              chart = "rook-ceph";
              version = "1.13.1";
              sha256 = "sha256-9oKp7OLoHxtGdon4epV0/BRkrgCjINXLYH6nKxL/pKU=";
            };
            values = {
              csi = let
                hostVolumes = [
                  {
                    name = "lib-modules";
                    hostPath.path = "/run/booted-system/kernel-modules/lib/modules/";
                  }
                  {
                    name = "host-nix";
                    hostPath.path = "/nix";
                  }
                ];
                hostVolumeMounts = [
                  {
                    name = "host-nix";
                    mountPath = "/nix";
                    readOnly = true;
                  }
                ];
              in {
                # -- The volume of the CephCSI RBD plugin DaemonSet
                csiRBDPluginVolume = hostVolumes;

                # -- The volume mounts of the CephCSI RBD plugin DaemonSet
                csiRBDPluginVolumeMount = hostVolumeMounts;

                # -- The volume of the CephCSI CephFS plugin DaemonSet
                csiCephFSPluginVolume = hostVolumes;

                # -- The volume mounts of the CephCSI CephFS plugin DaemonSet
                csiCephFSPluginVolumeMount = hostVolumeMounts;

                # NOTE Requires monitoring.nix to be deployed first
                monitoring.enabled = true;

                # Whether the helm chart should create and update the CRDs. If
                # false, the CRDs must be managed independently with
                # deploy/examples/crds.yaml. WARNING Only set during first
                # deployment
                #
                # Enable CRDs as nothing bad could happen to the cluster.
                crds.enabled = true;
              };
            };

            # There are various places in rook where this namespace is hard-coded
            namespace = "rook-ceph";
          };
          rook-ceph-cluster = {
            chart = kubenix.lib.helm.fetch {
              repo = "https://charts.rook.io/release";
              chart = "rook-ceph-cluster";
              version = "1.13.1";
              sha256 = "sha256-OAapsUJD2CP0ETX0vdnQfZChxJrypP3qEQAEB8hZ83A=";
            };
            values = {
              clusterName = "ceph";
              monitoring = {
                enabled = true;
                createPrometheusRules = true;
              };
              # https://rook.io/docs/rook/latest-release/CRDs/Cluster/external-cluster/
              cephClusterSpec = {
                crashCollector = {disable = true;};
                external = {enable = true;};
                cephVersion.image = "quay.io/ceph/ceph:v18.2.0"; # must be the same version from the system's nixpkgs
                healthCheck = {
                  daemonHealth = {
                    mon = {
                      disabled = false;
                      interval = "45s";
                    };
                  };
                };
              };
              cephBlockPools = {};
              # TODO how to create storage class without creating a pool
              #   storageClass.imageFeatures = "layering,fast-diff,object-map,deep-flatten,exclusive-lock";
              cephFileSystems = {};
              cephObjectStores = {};

              cephECStorageClass = {
                allowVolumeExpansion = true;
                name = "ceph-rbd-ec";
                parameters = {
                  clusterID = "rook-ceph";
                  # If you want to use erasure coded pool with RBD, you need to create
                  # two pools. one erasure coded and one replicated.
                  # You need to specify the replicated pool here in the `pool` parameter, it is
                  # used for the metadata of the images.
                  # The erasure coded pool must be set as the `dataPool` parameter below.
                  dataPool = "ec-data-pool";
                  imageFeatures = "layering,fast-diff,object-map,deep-flatten,exclusive-lock";
                  imageFormat = "2";
                  pool = "ec-metadata-pool";
                };
                provisioner = "rook-ceph.rbd.csi.ceph.com";
                reclaimPolicy = "Delete";
              };
            };
            namespace = "rook-ceph";
          };
        };
      }
      {
        resources.storageClasses = {
          ceph-rbd-test = {
            provisioner = "rook-ceph.rbd.csi.ceph.com";
            parameters = {
              clusterID = "rook-ceph";
              pool = "test";
              imageFeatures = "layering";
              "csi.storage.k8s.io/controller-expand-secret-name" = "rook-csi-rbd-provisioner";
              "csi.storage.k8s.io/controller-expand-secret-namespace" = "rook-ceph";
              "csi.storage.k8s.io/fstype" = "ext4";
              "csi.storage.k8s.io/node-stage-secret-name" = "rook-csi-rbd-node";
              "csi.storage.k8s.io/node-stage-secret-namespace" = "rook-ceph";
              "csi.storage.k8s.io/provisioner-secret-name" = "rook-csi-rbd-provisioner";
              "csi.storage.k8s.io/provisioner-secret-namespace" = "rook-ceph";
              imageFormat = "2";
            };
            allowVolumeExpansion = true;
            reclaimPolicy = "Delete";
          };

          ceph-rbd-ec = {
            provisioner = "rook-ceph.rbd.csi.ceph.com";
            parameters = {
              clusterID = "rook-ceph";
              dataPool = "ec-data-pool";
              pool = "ec-metadata-pool";
              imageFeatures = "layering,fast-diff,object-map,deep-flatten,exclusive-lock";
              "csi.storage.k8s.io/controller-expand-secret-name" = "rook-csi-rbd-provisioner";
              "csi.storage.k8s.io/controller-expand-secret-namespace" = "rook-ceph";
              "csi.storage.k8s.io/fstype" = "ext4";
              "csi.storage.k8s.io/node-stage-secret-name" = "rook-csi-rbd-node";
              "csi.storage.k8s.io/node-stage-secret-namespace" = "rook-ceph";
              "csi.storage.k8s.io/provisioner-secret-name" = "rook-csi-rbd-provisioner";
              "csi.storage.k8s.io/provisioner-secret-namespace" = "rook-ceph";
              imageFormat = "2";
            };
            allowVolumeExpansion = true;
            reclaimPolicy = "Delete";
          };

          cephfs = {
            provisioner = "rook-ceph.cephfs.csi.ceph.com";
            parameters = {
              clusterID = "rook-ceph";
              fsName = "cephfs";
              pool = "cephfs.cephfs.data";
              "csi.storage.k8s.io/provisioner-secret-name" = "rook-csi-cephfs-provisioner";
              "csi.storage.k8s.io/provisioner-secret-namespace" = "rook-ceph";
              "csi.storage.k8s.io/controller-expand-secret-name" = "rook-csi-cephfs-provisioner";
              "csi.storage.k8s.io/controller-expand-secret-namespace" = "rook-ceph";
              "csi.storage.k8s.io/node-stage-secret-name" = "rook-csi-cephfs-node";
              "csi.storage.k8s.io/node-stage-secret-namespace" = "rook-ceph";
            };
            allowVolumeExpansion = true;
            reclaimPolicy = "Delete";
          };
        };
      }
      {
        resources = {
          # service-monitor.yaml
          serviceMonitors = {
            rook-ceph-mgr.metadata.labels.team = "rook";
            rook-ceph-mgr.spec = {
              endpoints = [
                {
                  interval = "5s";
                  path = "/metrics";
                  port = "http-metrics";
                }
              ];
              namespaceSelector = {matchNames = ["rook-ceph"];};
              selector = {
                matchLabels = {
                  app = "rook-ceph-mgr";
                  rook_cluster = "rook-ceph";
                };
              };
            };
            # exporter-service-monitor.yaml
            rook-ceph-exporter.metadata.labels.team = "rook";
            rook-ceph-exporter.spec = {
              endpoints = [
                {
                  interval = "5s";
                  path = "/metrics";
                  port = "ceph-exporter-http-metrics";
                }
              ];
              namespaceSelector = {matchNames = ["rook-ceph"];};
              selector = {
                matchLabels = {
                  app = "rook-ceph-exporter";
                  ceph_daemon_id = "exporter";
                  rook_cluster = "rook-ceph";
                };
              };
            };
          };
          # prometheus-service.yaml
          # FIXME how to fix selector kapp.k14s.io/app=N not updating
          # Selector:                 kapp.k14s.io/app=1704800032682165538,prometheus=rook-prometheus
          services.rook-ceph.spec = {
            ports = [
              {
                name = "web";
                nodePort = 30900;
                port = 9090;
                protocol = "TCP";
                targetPort = "web";
              }
            ];
            selector = {prometheus = "rook-prometheus";};
            type = "NodePort";
          };
          # prometheus.yaml
          serviceAccounts.prometheus = {};
          clusterRoles.prometheus = {
            aggregationRule.clusterRoleSelectors = [{matchLabels = {"rbac.ceph.rook.io/aggregate-to-prometheus" = "true";};}];
            rules = [];
          };
          clusterRoles.prometheus-rules = {
            metadata.labels = {"rbac.ceph.rook.io/aggregate-to-prometheus" = "true";};
            rules = [
              {
                apiGroups = [""];
                resources = ["nodes" "services" "endpoints" "pods"];
                verbs = ["get" "list" "watch"];
              }
              {
                apiGroups = [""];
                resources = ["configmaps"];
                verbs = ["get"];
              }
              {
                nonResourceURLs = ["/metrics"];
                verbs = ["get"];
              }
            ];
          };
          clusterRoleBindings.prometheus = {
            roleRef = {
              apiGroup = "rbac.authorization.k8s.io";
              kind = "ClusterRole";
              name = "prometheus";
            };
            subjects = [
              {
                kind = "ServiceAccount";
                name = "prometheus";
                namespace = "rook-ceph";
              }
            ];
          };
          prometheuses.rook-prometheus = {
            metadata.labels.prometheus = "rook-prometheus";
            spec = {
              resources = {requests = {memory = "400Mi";};};
              ruleSelector = {
                matchLabels = {
                  prometheus = "rook-prometheus";
                  role = "alert-rules";
                };
              };
              serviceAccountName = "prometheus";
              serviceMonitorSelector = {matchLabels = {team = "rook";};};
            };
          };
        };
      }
      {
        resources = {
          # Monitoring related settings
          #
          # roles = {
          #   rook-ceph-monitor.rules = [
          #     {
          #       apiGroups = ["monitoring.coreos.com"];
          #       resources = ["servicemonitors"];
          #       verbs = ["get" "list" "watch" "create" "update" "delete"];
          #     }
          #   ];
          #   rook-ceph-metrics = {
          #     rules = [
          #       {
          #         apiGroups = [""]; # FIXME is this a mistake?
          #         resources = ["services" "endpoints" "pods"];
          #         verbs = ["get" "list" "watch"];
          #       }
          #     ];
          #   };
          #   rook-ceph-monitor-mgr = {
          #     rules = [
          #       {
          #         apiGroups = ["monitoring.coreos.com"];
          #         resources = ["servicemonitors"];
          #         verbs = ["get" "list" "create" "update"];
          #       }
          #     ];
          #   };
          # };
          # roleBindings = {
          #   rook-ceph-monitor = {
          #     roleRef = {
          #       apiGroup = "rbac.authorization.k8s.io";
          #       kind = "Role";
          #       name = "rook-ceph-monitor";
          #     };
          #     subjects = [
          #       {
          #         kind = "ServiceAccount";
          #         name = "rook-ceph-system";
          #         namespace = "rook-ceph";
          #       }
          #     ];
          #   };
          #   rook-ceph-metrics = {
          #     roleRef = {
          #       apiGroup = "rbac.authorization.k8s.io";
          #       kind = "Role";
          #       name = "rook-ceph-metrics";
          #     };
          #     subjects = [
          #       {
          #         kind = "ServiceAccount";
          #         name = "prometheus-k8s";
          #         namespace = "rook-ceph";
          #       }
          #     ];
          #   };
          #   rook-ceph-monitor-mgr = {
          #     roleRef = {
          #       apiGroup = "rbac.authorization.k8s.io";
          #       kind = "Role";
          #       name = "rook-ceph-monitor-mgr";
          #     };
          #     subjects = [
          #       {
          #         kind = "ServiceAccount";
          #         name = "rook-ceph-mgr";
          #         namespace = "rook-ceph";
          #       }
          #     ];
          #   };
          # };

          # prometheusRules.prometheus-ceph-rules = import ./prometheus-ceph-rule.nix;
        };
      }
      args.kubernetes
    ];
  };
}
