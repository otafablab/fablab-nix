{
  config,
  modulesPath,
  ...
}: {
  # TODO unify with hosting
  imports = [
    (modulesPath + "/installer/netboot/netboot.nix")
    (modulesPath + "/profiles/all-hardware.nix")
    ../modules/admins.nix
    ../modules/core.nix
    ../modules/autoUpdate.nix
    ../modules/fileSystems.nix
    ../modules/openssh.nix
  ];

  networking = {
    # You should try to make this ID unique among your machines. The primary use
    # case is to ensure when using ZFS that a pool isn’t imported accidentally on
    # a wrong machine.
    hostId = "00000000";

    networkmanager.enable = true;

    # TODO enable jumbo frames?
  };

  # systemd-networkd-wait-online can timeout and fail if there are no network
  # interfaces available for it to manage. When systemd-networkd is enabled but
  # a different service is responsible for managing the system’s internet
  # connection (for example, NetworkManager or connman are used to manage WiFi
  # connections), this service is unnecessary and can be disabled.
  systemd.network.wait-online.enable = false;

  sops = {
    defaultSopsFile = ../secrets/kube.yaml;
    secrets."k3s/short-token" = {};
  };

  zramSwap.enable = true;

  system.stateVersion = config.system.nixos.release;
}
