{
  config,
  lib,
  pkgs,
  ...
}: let
  clusterCIDR = "10.44.0.0/16";
in {
  imports = [
    ./containerd.nix
    ./node-password.nix
  ];

  environment.systemPackages = with pkgs; [
    kubectl
    # kubernetes
  ];

  networking = {
    useNetworkd = true;

    firewall.allowedTCPPorts = [
      10250 # k3s: kubelet metrics
      6443 # k3s: required so that pods can reach the API server (running on port 6443 by default)
      2379 # k3s, etcd clients: required if using a "High Availability Embedded etcd" configuration
      2380 # k3s, etcd peers: required if using a "High Availability Embedded etcd" configuration
      9100 # prometheus node exporter daemonset
    ];
    firewall.allowedUDPPorts = [
      8472 # k3s, flannel: required if using multi-node for inter-node networking
    ];
  };

  services.k3s = {
    enable = true;
    role = "server"; # server runs etcd and api server
    # TODO not supported by kubechad
    tokenFile = config.sops.secrets."k3s/short-token".path;
    clusterInit = true;
    serverAddr = lib.mkIf (config.networking.hostName != "amdahl") "https://10.42.0.11:6443"; # Address of amdahl

    extraFlags = toString [
      "--container-runtime-endpoint unix:///run/containerd/containerd.sock"
      "--disable=traefik"
      # "--flannel-backend=wireguard-native" # TODO https://docs.k3s.io/installation/network-options?_highlight=wireguard#flannel-options
      "--cluster-cidr=${clusterCIDR}"
    ];
  };
}
