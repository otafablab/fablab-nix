{
  config,
  lib,
  ...
}: {
  imports = [
    ./containerd.nix
    ./node-password.nix
  ];

  networking = {
    useNetworkd = true;

    firewall.allowedTCPPorts = [
      10250 # k3s: kubelet metrics
      9100 # prometheus node exporter daemonset
    ];
    firewall.allowedUDPPorts = [
      8472 # k3s, flannel: required if using multi-node for inter-node networking
    ];
  };

  services.k3s = {
    enable = true;
    role = "agent"; # server runs etcd and api server
    tokenFile = config.sops.secrets."k3s/short-token".path;
    serverAddr = lib.mkIf (config.networking.hostName != "amdahl") "https://10.42.0.11:6443"; # Address of amdahl

    extraFlags = toString [
      "--container-runtime-endpoint unix:///run/containerd/containerd.sock"
      # "--flannel-backend=wireguard-native" # TODO https://docs.k3s.io/installation/network-options?_highlight=wireguard#flannel-options
    ];
  };
}
