{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
    flake-utils.url = "github:numtide/flake-utils";

    kubenix = {
      url = "github:xhalo32/kubenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    nixpkgs,
    flake-utils,
    kubenix,
    ...
  }:
    with flake-utils.lib;
      eachSystem allSystems (system: let
        # TODO create a constants file for unifying these
        host = "fablab-systems.fi";

        fablab-management = kubenix.evalModules.${system} {
          module = {kubenix, ...}: {
            imports = with kubenix.modules; [submodules k8s];
            submodules.imports = [./cluster/fablab-management];
            submodules.instances.fablab-management = {
              submodule = "fablab-management";
              args = {
                inherit host;
                management-domain = "fablab-management.${host}";
              };
            };

            kubenix.project = "fablab-nix";
            kubernetes.version = "1.27";
          };
        };
        rook-ceph-ext = kubenix.evalModules.${system} {
          module = {kubenix, ...}: {
            imports = with kubenix.modules; [submodules k8s];
            submodules.imports = [./cluster/rook-ceph-ext.nix];
            submodules.instances.rook-ceph-ext = {
              submodule = "rook-ceph-ext";
            };

            kubenix.project = "fablab-nix";
            kubernetes.version = "1.27";
          };
        };
        monitoring = kubenix.evalModules.${system} {
          module = {kubenix, ...}: {
            imports = with kubenix.modules; [submodules k8s];
            submodules.imports = [./cluster/monitoring.nix];
            submodules.instances.monitoring = {
              submodule = "monitoring";
            };

            kubenix.project = "fablab-nix";
            kubernetes.version = "1.27";
          };
        };
        cert-manager = kubenix.evalModules.${system} {
          module = {kubenix, ...}: {
            imports = with kubenix.modules; [submodules k8s];
            submodules.imports = [./cluster/cert-manager];
            submodules.instances.cert-manager = {
              submodule = "cert-manager";
            };

            kubenix.project = "fablab-nix";
            kubernetes.version = "1.27";
          };
        };
        pronter-ctrl = kubenix.evalModules.${system} {
          module = {kubenix, ...}: {
            imports = with kubenix.modules; [submodules k8s];
            submodules.imports = [./cluster/pronter-ctrl];
            submodules.instances.pronter-ctrl = {
              submodule = "pronter-ctrl";
              args = {
                inherit host;
                management-domain = "fablab-management.${host}";
                klipperImage = klipper.config.docker.images.klipper.path;
              };
            };

            kubenix.project = "fablab-nix";
            kubernetes.version = "1.27";
          };
        };
        klipper = kubenix.evalModules.${system} {
          module = {kubenix, ...}: let
            pkgs = import nixpkgs {
              inherit system;
              overlays = [
                (_final: prev: {
                  klipper = prev.klipper.overrideAttrs (_old: {
                    src = prev.fetchFromGitHub {
                      owner = "xhalo32";
                      repo = "klipper";
                      rev = "49d7306df2a2f0720f73cb61f6e348758ffad385";
                      hash = "sha256-sdeGfe2cTTGHp4GDaHPeRGyVovFQcTiFMMSlVMWgSAA=";
                    };
                  });
                })
              ];
            };
          in {
            imports = with kubenix.modules; [docker];
            docker = {
              registry.url = "registry.fablab-systems.fi";
              images.klipper.image = pkgs.callPackage ./cluster/pronter-ctrl/klipper.nix {};
            };
          };
        };
        ingress-nginx = kubenix.evalModules.${system} {
          module = {kubenix, ...}: {
            imports = with kubenix.modules; [submodules k8s];
            submodules.imports = [./cluster/ingress-nginx.nix];
            submodules.instances.ingress-nginx = {
              submodule = "ingress-nginx";
            };

            kubenix.project = "fablab-nix";
            kubernetes.version = "1.27";
          };
        };
      in {
        packages = {
          fablab-management = fablab-management.config.kubernetes.result;
          rook-ceph-ext = rook-ceph-ext.config.kubernetes.result;
          monitoring = monitoring.config.kubernetes.result;
          cert-manager = cert-manager.config.kubernetes.result;
          # Build kubernetes manifests: nix build .#pronter-ctrl.kubernetes.result
          # Load klipper to podman: podman load -i (nix build .#pronter-ctrl.docker.images.klipper.image --print-out-paths)
          # TODO not a derivation
          pronter-ctrl = pronter-ctrl.config.kubernetes.result;
          klipper = klipper.config.docker.images.klipper.image;
          ingress-nginx = ingress-nginx.config.kubernetes.result;
        };
      });
}
