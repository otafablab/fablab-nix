{
  writeTextDir,
  pkgs,
}: let
  nginxConf = writeTextDir "etc/nginx/nginx.conf" ''
    user nginx nginx;
    daemon off;
    error_log /dev/stdout info;
    pid /dev/null;
    events {}
    http {
      include /etc/nginx/upstreams.conf;
      access_log /dev/stdout;
      map $http_upgrade $connection_upgrade {
        default upgrade;
        ''\'''\' close;
      }
      server {
        listen 80;
        root ${pkgs.mainsail}/share/mainsail;
        gzip on;
        gzip_vary on;
        gzip_proxied any;
        gzip_proxied expired no-cache no-store private auth;
        gzip_comp_level 4;
        gzip_buffers 16 8k;
        gzip_http_version 1.1;
        gzip_types text/plain text/css text/xml text/javascript application/javascript application/x-javascript application/json application/xml;
        index index.html;
        client_max_body_size 0;
        proxy_request_buffering off;
        location / {
          try_files $uri $uri/ /index.html;
        }
        location = /index.html {
          add_header Cache-Control "no-store, no-cache, must-revalidate";
        }
        location /websocket {
          proxy_pass http://apiserver/websocket;
          proxy_http_version 1.1;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection $connection_upgrade;
          proxy_set_header Host $http_host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_read_timeout 86400;
        }
        location ~ ^/(printer|api|access|machine|server)/ {
          proxy_pass http://apiserver$request_uri;
          proxy_http_version 1.1;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Host $http_host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header X-Scheme $scheme;
        }
        location ~* \.js$ {
          types { }
          default_type "text/javascript; charset=utf-8";
        }
        location ~* \.css$ {
          types { }
          default_type "text/css; charset=utf-8";
        }
      }
    }'';
in
  pkgs.dockerTools.buildImage {
    name = "registry.gitlab.com/otafablab/fablab-nix/mainsail";
    tag = "latest";
    copyToRoot = pkgs.buildEnv {
      name = "image-root";
      # TODO remove bloat pkgs
      paths = [nginxConf pkgs.coreutils-full pkgs.bashInteractive];
    };
    extraCommands = ''
      mkdir -p {etc,tmp}
      chmod u+w {etc,tmp}
      echo "nginx:x:1000:1000::/:" > etc/passwd
      echo "nginx:x:1000:nginx" > etc/group
    '';
    config = {
      Cmd = ["${pkgs.nginx}/bin/nginx" "-c" "/etc/nginx/nginx.conf"];
      ExposedPorts."80/tcp" = {};
    };
  }
