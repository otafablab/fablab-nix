#!/usr/bin/env sh
# Use this script to clean up the mess rook leaves behind.
# <https://rook.io/docs/rook/v1.12/Getting-Started/ceph-teardown/>
#
# Run on multiple hosts using `pssh -IPH "edelman fredkin gabriel" < cleanup-rook-ceph.sh`

rm -rf /var/lib/rook
echo "/var/lib/rook deleted"

nix shell nixpkgs#gptfdisk -c sgdisk --zap-all /dev/sda
nix shell nixpkgs#gptfdisk -c sgdisk --zap-all /dev/sdb

dd if=/dev/zero of=/dev/sda bs=1M count=100 oflag=direct,dsync &
dd if=/dev/zero of=/dev/sdb bs=1M count=100 oflag=direct,dsync &

wait

lsblk -f |grep -v ^nbd
