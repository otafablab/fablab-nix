{
  config,
  modulesPath,
  ...
}:
# A configuration for those machines which are not part of the network but PXE
# boot in it anyways. Note that /var disk is still required
{
  imports = [
    (modulesPath + "/installer/netboot/netboot.nix")
    (modulesPath + "/profiles/all-hardware.nix")
    ./modules/admins.nix
    ./modules/core.nix
    # auto update also makes sense for random machines as they might get a hostname at some point and therefore know which configuration to build
    ./modules/autoUpdate.nix
    ./modules/fileSystems.nix
    ./modules/openssh.nix
  ];
  nixpkgs.hostPlatform = "x86_64-linux";
  networking = {
    networkmanager.enable = true;
  };

  systemd.network.wait-online.enable = false;

  zramSwap.enable = true;

  system.stateVersion = config.system.nixos.release;
}
