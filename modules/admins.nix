{pkgs, ...}: let
  keyFiles = with (import ./utils.nix pkgs); [
    ../keys/users/niklashh
    ../keys/users/naatulus
    ../keys/users/sivari
    (fetchKeys "mkez" "sha256-l0PZnoH52CRpod1wX6s627mekHk0UbkOhpQJ7XksENA=")
  ];
in {
  users.users.root.openssh.authorizedKeys.keyFiles = keyFiles;
}
