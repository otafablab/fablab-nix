{config, ...}: {
  # pxe nodes (which haven't set their hostname) should rebuild to correct
  # configuration right after booting. the new configuration has the hostname set so this is skipped
  # TODO however it takes 3s to set the hostname by DHCP. the hostname is set by systemd-hostnamed.service
  #
  # FIXME doesn't work because it stops/removes itself
  # rebuild-after-hostname-start[10869]: stopping the following units:
  # systemd[1]: Stopping rebuild-after-hostname.service...
  systemd.services.rebuild-after-hostname = {
    #lib.mkIf (config.networking.hostName == "") {
    after = ["systemd-hostnamed.service"];
    wants = ["systemd-hostnamed.service"];
    wantedBy = ["multi-user.target"];
    serviceConfig = {
      Type = "simple";
      Restart = "on-failure";
      RestartSec = 8;
    };
    #script = ''${config.system.build.nixos-rebuild}/bin/nixos-rebuild switch --flake gitlab:otafablab/fablab-nix --refresh'';
  };
  system.autoUpgrade = {
    # FIXME reboot only if required
    # allowReboot = true; # reboots every time regardless of whether kernel version changed
    # rebootWindow = {
    #   lower = "00:00";
    #   upper = "24:00";
    # };
    randomizedDelaySec = "2min";
    enable = true;
    # pxe nodes should rebuild only after the rtr has finished rebuilding. building the squashfs takes ~10min
    dates =
      if config.networking.hostName == "rtr"
      then "02:00"
      else "02:30";
    flags = ["--refresh"];
    flake = "gitlab:otafablab/fablab-nix";
  };
}
