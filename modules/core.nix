{
  pkgs,
  inputs,
  ...
}: {
  i18n.defaultLocale = "en_US.UTF-8";
  time.timeZone = "Europe/Helsinki";

  console = {
    useXkbConfig = true;
  };

  nix = {
    gc = {
      persistent = true;
      automatic = true;
      dates = "daily";
      options = "--delete-older-than 7d";
    };

    settings = {
      keep-outputs = true;
      keep-derivations = true;
      experimental-features = ["nix-command" "flakes"];
      auto-optimise-store = true;
      # trusted-substituters = [
      #   "http://binarycache.10.42.0.1.nip.io"
      # ];
      # trusted-public-keys = [
      #   "binarycache.10.42.0.1.nip.io-1:rPbKldhtVp9pCwPcAr1Vc/+ktj9cLDqbFwDxHXg700M="
      # ];
    };
  };

  environment.systemPackages = with pkgs;
    [
      htop
      emacs-nox
      neovim
      nmap
      iotop
      pciutils
      ssh-to-age
      tmux
    ]
    ++ (with inputs.self.packages."x86_64-linux"; [
      # Packages from current flake outputs
      hw-info
      upgrade
      setupVar
    ]);

  users.mutableUsers = false;
}
