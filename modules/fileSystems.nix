_: {
  fileSystems."/var" = {
    label = "var";
    fsType = "ext4";
    options = [
      "defaults"
      "noatime"
    ];
  };
}
