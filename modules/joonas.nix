{pkgs, ...}: {
  users.users.joonas = {
    isNormalUser = true;
    openssh.authorizedKeys.keyFiles = with (import ./utils.nix pkgs); [(fetchKeysGitHub "Joonas-vonlerber" "sha256-yGeSlxDeXGZju7xj2nmAz9z7tX1hkevD8hcWdelhJks=")];
    shell = pkgs.fish;
    extraGroups = ["wheel"];
    home = "/var/home/joonas";
  };
  programs.fish.enable = true;

  environment.systemPackages = with pkgs; [
    hashcat
    cudatoolkit
  ];
}
