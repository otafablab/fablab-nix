{ config, ... }:
{
  services.xserver.videoDrivers = [ "nvidia" ];
  hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.stable;
  boot.blacklistedKernelModules = [ "nouveau" ];
  hardware.opengl = {
    enable = true;
  };
}
