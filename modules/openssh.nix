_: {
  services.openssh = {
    enable = true;
    hostKeys = [
      {
        path = "/var/ssh_host_ed25519_key";
        type = "ed25519";
      }
    ];
  };
}
