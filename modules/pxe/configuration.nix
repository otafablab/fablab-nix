# A basic PXE image that every machine boots
{
  config,
  modulesPath,
  ...
}: {
  imports = [
    (modulesPath + "/installer/netboot/netboot.nix")
    (modulesPath + "/profiles/all-hardware.nix")
    ../../modules/core.nix
    ../../modules/autoUpdate.nix
    ../../modules/admins.nix
    ../../modules/fileSystems.nix
    ../../modules/openssh.nix
  ];

  # https://github.com/NixOS/nixpkgs/pull/94660
  system.build.isoImage.squashfsCompression = "zstd -Xcompression-level 6";

  # Use faster squashfs compression
  # system.build.isoImage.squashfsCompression = "gzip -Xcompression-level 1";

  # [*] R420
  # megaraid_sas          180224  1
  # scsi_mod              274432  5 sd_mod,usb_storage,uas,megaraid_sas,libata
  #
  # [*] R410
  # mptsas                 53248  1
  # mptscsih               32768  1 mptsas
  # mptbase                77824  2 mptsas,mptscsih
  # scsi_transport_sas     49152  1 mptsas
  # scsi_mod              274432  6 mptsas,scsi_transport_sas,sd_mod,mptscsih,libata,sr_mod
  # scsi_common            16384  5 mptsas,scsi_mod,mptscsih,libata,sr_mod
  #
  # [*] UCS220M3S
  # mpt3sas               352256  0
  # raid_class             16384  1 mpt3sas
  # scsi_transport_sas     49152  1 mpt3sas
  # scsi_mod              274432  6 scsi_transport_sas,sd_mod,raid_class,usb_storage,uas,mpt3sas
  # scsi_common            16384  4 scsi_mod,usb_storage,uas,mpt3sas
  boot.initrd.availableKernelModules = ["megaraid_sas" "mptsas" "mpt3sas"];
  boot.extraModprobeConfig = "options kvm_intel nested=1";

  networking = {
    # You should try to make this ID unique among your machines. The primary use
    # case is to ensure when using ZFS that a pool isn’t imported accidentally on
    # a wrong machine.
    hostId = "00000000";

    # The name of the machine. Leave it empty if you want to obtain it from a DHCP
    # server (if using DHCP).
    hostName = "";

    networkmanager.enable = true;
  };

  zramSwap.enable = true;

  system.stateVersion = config.system.nixos.release;
}
