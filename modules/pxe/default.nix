{
  config,
  pkgs,
  inputs,
  ...
}: let
  sys = inputs.nixpkgs.lib.nixosSystem {
    inherit pkgs;
    system = "x86_64-linux";

    specialArgs = {inherit inputs;};

    modules = [
      inputs.sops-nix.nixosModules.sops
      ./configuration.nix
    ];
  };

  inherit (sys.config.system) build;
in {
  networking.firewall.allowedTCPPorts = [81];
  networking.firewall.allowedUDPPorts = [67 69 4011];
  systemd.services.pixiecore = {
    enable = true;
    description = "Fablab PXE server";
    serviceConfig = {
      # NOTE Add `boot.trace` to cmdline if more debug is required
      ExecStart = ''${pkgs.pixiecore}/bin/pixiecore boot ${build.kernel}/bzImage ${build.netbootRamdisk}/initrd --cmdline "init=${build.toplevel}/init loglevel=4 boot.shell_on_fail edd=off" --debug --dhcp-no-bind --port 81 --status-port 81'';
    };
    wantedBy = ["multi-user.target"];
  };
}
