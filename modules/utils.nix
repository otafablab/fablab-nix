{fetchurl, ...}: {
  fetchKeys = username: hash: (fetchurl {
    sha256 = hash;
    url = "https://gitlab.com/${username}.keys";
  });
  fetchKeysGitHub = username: hash: (fetchurl {
    sha256 = hash;
    url = "https://github.com/${username}.keys";
  });
}
