* Fablab raspberry pi NixOS infrastructure
** Overview
The fablab 3D printer controller raspberry pi's run NixOS, the superior operating system, along with klipper, mainsail and moonraker.

The Ultimakers are flashed with klipper firmware. Klipper configuration can be found here: [[./ultimaker-2-plus.cfg]].

** Flashing
To flash the ultimaker with klipper firmware, follow the [[https://www.klipper3d.org/Installation.html][official guide]]. Note that ~install-octopi.sh~ also install some libraries, but don't run the script, instead install these directly.

#+begin_src sh
apt install --yes libffi-dev build-essential libncurses-dev libusb-dev avrdude gcc-avr binutils-avr avr-libc
#+end_src

If you don't have a ubuntu machine available, try running one in a container using [[https://github.com/89luca89/distrobox][distrobox]].

The make flash command works without sudo as long as you give everyone write access: ~sudo chmod o+rw /dev/ttyACM0~.

** Installation
First install NixOS on the raspberry pi. Then run ~nixos-rebuild switch --flake gitlab:otafablab/fablab-nix#fablab-raspberrypi-N~ where N is replaced with the corresponding index.

The system should rebuild itself every hour if new commits are added to the repository.

On successive installs the ~#...~ can be left out as it is inferred from the hostname.

** Hacking
Klipper configuration can be set to a writable location, so that it can be modified during runtime if necessary. Just set ~mutableConfig = true;~ in the klipper nix options.

The workflow to synchronize whole OS to the pi is as follows:
- ~rsync -av {flake.*,rpi} root@10.42.0.198:/etc/nixos/~
- ~nixos-rebuild switch --flake /etc/nixos~

To only set up the klipper configuration
- ~rsync -av rpi root@10.42.0.198:/etc/nixos/~
- ~cat /etc/nixos/rpi/{base,ultimaker-$(hostname)-overrides}.cfg > /var/lib/klipper/printer.cfg && systemctl restart klipper~

** TODOs
*** Create a portal system to access the printers
**** Access control with Keycloak
*** Update nixpkgs klipper upstream
*** Improve klipper module to support options
*** Add firmware generation support to klipper module
**** Maybe even automatic flashing?
*** lol
