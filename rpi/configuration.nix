{
  config,
  lib,
  ...
}: let
  # Only enable auto upgrade if current config came from a clean tree
  # This avoids accidental auto-upgrades when working locally.
  # Credits to https://git.sr.ht/~misterio
  klipperBase = name: {
    user = "klipper";
    group = "klipper";
    settings =
      lib.recursiveUpdate
      (import ./ultimaker-2-plus.nix)
      {
        virtual_sdcard.path = "/var/lib/moonraker-${name}/gcodes"; # TODO make more functional
      };
  };
  moonrakerBase = _name: {
    user = "klipper";
    group = "klipper";
    settings = {
      file_manager.enable_object_processing = false;
      authorization.cors_domains = [
        "*.local"
        "*.lan"
        "*://localhost"
        "*"
      ];
      authorization.trusted_clients = [
        "10.0.0.0/8"
        "127.0.0.0/8"
        "169.254.0.0/16"
        "172.16.0.0/12"
        "192.168.0.0/16"
        "FE80::/10"
        "::1/128"
      ];
      server = {
        host = "127.0.0.1";
      };
    };
  };
in {
  boot = {
    # NixOS wants to enable GRUB by default
    loader.grub.enable = false;
    # Enables the generation of /boot/extlinux/extlinux.conf
    loader.generic-extlinux-compatible.enable = true;

    kernelParams = ["cma=320M"];
  };

  imports = [
    ./hardware-configuration.nix
    ./klippers.nix
    ./moonrakers.nix
    ./mainsail.nix
    ../modules/admins.nix
  ];

  hardware.enableRedistributableFirmware = true;
  networking.wireless.enable = true;
  time.timeZone = "Europe/Helsinki";

  services.openssh = {
    enable = true;
    settings.PermitRootLogin = "yes";
  };

  users = {
    users = {
      root.initialPassword = "a";
      klipper = {
        isSystemUser = true;
        group = "klipper";
      };
    };
    groups.klipper = {};
  };

  niklash.services.klippers = builtins.map (a: lib.recursiveUpdate (klipperBase a.name) a) [
    {
      name = "klipper1";
      settings.mcu.serial = "/dev/serial/by-path/platform-3f980000.usb-usb-0:1.2:1.0";
    }
    {
      name = "klipper2";
      settings.mcu.serial = "/dev/serial/by-path/platform-3f980000.usb-usb-0:1.3:1.0";
    }
  ];
  # TODO add support for configuring multiple moonraker upstreams
  # The REST endpoint to get the mainsail related data is http://localhost/server/database/item?namespace=mainsail
  niklash.services.moonrakers = lib.lists.imap0 (idx: a: let
    name = "moonraker-${a.name}";
  in
    lib.recursiveUpdate (moonrakerBase name) {
      inherit name;
      klipperSocket = "/run/${a.name}/api";
      settings.server.port = builtins.toString (7125 + idx);
    })
  config.niklash.services.klippers;

  # niklash.services.mainsail.enable = true;
  services.mainsail = {
    enable = true;
  };

  zramSwap.enable = true;

  swapDevices = [
    {
      device = "/var/lib/swapfile";
      size = 2 * 1024; # 2GiB
    }
  ];

  nix = {
    gc = {
      persistent = true;
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 14d";
    };
    settings = {
      keep-outputs = true;
      keep-derivations = true;
      experimental-features = ["nix-command" "flakes"];
      auto-optimise-store = true;
    };
  };

  system.autoUpgrade = {
    enable = true;
    dates = "hourly";
    flags = ["--refresh"];
    flake = "gitlab:otafablab/fablab-nix";
  };

  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}
