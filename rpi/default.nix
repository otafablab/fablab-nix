{nixpkgs, ...} @ inputs: {
  fablab-raspberrypi-3 = nixpkgs.lib.nixosSystem {
    specialArgs = {inherit inputs;};
    modules = [
      ./configuration.nix
      {
        networking.hostName = "fablab-raspberrypi-3";
      }
    ];
  };
  fablab-raspberrypi-2 = nixpkgs.lib.nixosSystem {
    specialArgs = {inherit inputs;};
    modules = [
      ./configuration.nix
      {
        networking.hostName = "fablab-raspberrypi-2";
      }
    ];
  };
  fablab-raspberrypi-1 = nixpkgs.lib.nixosSystem {
    specialArgs = {inherit inputs;};
    modules = [
      ./configuration.nix
      {
        networking.hostName = "fablab-raspberrypi-1";
      }
    ];
  };
}
