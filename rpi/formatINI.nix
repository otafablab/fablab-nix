{lib, ...}:
with lib; let
  addIndent = {
    indentation ? "\t",
    stripPrefix ? true,
  }: str:
    (
      if stripPrefix
      then lib.strings.removePrefix indentation
      else id
    ) (lib.strings.concatMapStringsSep "\n" (line: "${indentation}${line}") (lib.strings.splitString "\n" str));
in
  # TODO why did I need to create custom serialization for this?
  lib.generators.toINI {
    mkKeyValue = lib.generators.mkKeyValueDefault {mkValueString = addIndent {indentation = "  ";};} ":";
  }
