#!/usr/bin/env python
# Simple script to convert ini configuration to a nix attrset

import configparser as cp
import sys
from textwrap import indent

def main():
    if len(sys.argv) < 2:
        print('Missing argument file path', file=sys.stderr)
        sys.exit(1)
    config = cp.ConfigParser(interpolation=None) # Disable % interpolation or errors will come up
    config.read(sys.argv[1])

    print('# generated using ini2nix.py\n{')
    for s in config.sections():
        print(f'  "{s}" = {{')
        for (k, v) in config.items(s):
            ind = ' ' * 6
            v = indent(v, ind)
            v = v.strip() # TODO add flag for this
            assert("''" not in v)
            assert("$" not in v)
            assert("#" not in v)
            print(f'    "{k}" = ', end='')
            if '\n' in v or "'" in v:
                print(f"''\n{ind}{v}\n    '';")
            else:
                print(f'"{v}";')
                
        print('  };')
    print('}')

if __name__ == '__main__':
    main()

# TODO
# - add support for comments
# - create a dedicated serializer for nix
# - rwir
