{
  lib,
  pkgs,
  modulesPath,
  ...
}: let
  klipper = _c:
    lib.evalModules {
      specialArgs = {inherit pkgs;};
      modules = [
        "${modulesPath}/services/misc/klipper.nix"
        {
          config.services.klipper = {
            enable = true;
            octoprintIntegration = false;
            user = "klipper";
            group = "klipper";
            firmwares = {};
            configFile = ./.;
            settings = {};
          };
          # config.services.klipper = c;
        }
      ];
    };
in {
  # options = {

  # }
  systemd.services.klipper-1 =
    builtins.trace
    (klipper {
      inputTTY = "/run/klipper-1/tty";
      apiSocket = "/run/klipper-1/api";
    })
    .config
    .systemd
    .services
    .klipper {};
  # systemd.services.klipper-2 = (klipper {
  #   inputTTY = "/run/klipper-2/tty";
  #   apiSocket = "/run/klipper-2/api";
  # }).config.systemd.services.klipper // {
  #   serviceConfig.RuntimeDirectory = "klipper-2";
  #   serviceConfig.StateDirectory = "klipper-2";
  # };

  # environment.systemPackages = (klipper { enable = true; }).config.environment.systemPackages;
}
