{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}:
with lib; let
  cfg = config.niklash.klippers;
  klipperOptions =
    (import "${modulesPath}/services/misc/klipper.nix" {
      inherit lib pkgs;
      config = {services.klipper = cfg;};
    })
    .options;
  klipper = lib.evalModules {
    modules = [
      "${modulesPath}/services/misc/klipper.nix"
      {services.klipper = cfg;}
    ];
    specialArgs = {inherit pkgs lib;};
  };
in {
  ##### interface
  options.niklash.klippers = mkOption {
    type = types.attrsOf (types.submodule klipperOptions);
  };

  ##### implementation
  config = {
    assertions = [
      # TODO assert klipper paths don't conflict
    ];
    systemd.services.klipper1 = builtins.trace klipper.config.content.systemd.services.klipper null;
  };
}
