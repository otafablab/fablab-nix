_final: prev: {
  # Update klipper to same revision as what is flashed.
  # This in particular supports the config_i2c command.
  klipper = prev.klipper.overrideAttrs (_oldAttrs: {
    src = prev.fetchFromGitHub {
      owner = "Klipper3d";
      repo = "klipper";
      rev = "5f990f93d533247d3a675e8c423280f4333ad8ce";
      sha256 = "sha256-jICOEzLvy2wBInW4qIbFZbhRuHjsio6UM13K9UlZi1U=";
    };
  });
}
