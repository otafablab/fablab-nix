{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}:
with lib; let
  cfg = config.niklash.services.klippers;
  format = pkgs.formats.ini {
    # https://github.com/NixOS/nixpkgs/pull/121613#issuecomment-885241996
    listToValue = l:
      if builtins.length l == 1
      then generators.mkValueStringDefault {} (head l)
      else lib.concatMapStrings (s: "\n  ${generators.mkValueStringDefault {} s}") l;
    mkKeyValue = generators.mkKeyValueDefault {} ":";
  };
in {
  ##### interface
  options = {
    niklash.services.klippers = mkOption {
      default = {};
      type = types.attrsOf (types.submodule (import "${modulesPath}/services/misc/klipper.nix" {
        inherit pkgs lib;
        config = config // {};
      }));
    };
  };

  ##### implementation
  config = {
    nixpkgs.overlays = [(import ./klipper-overlay.nix)];

    systemd.services =
      builtins.mapAttrs (_name: cfg: let
        klippyArgs =
          "--input-tty=${cfg.inputTTY}"
          + optionalString (cfg.apiSocket != null) " --api-server=${cfg.apiSocket}"
          + optionalString (cfg.logFile != null) " --logfile=${cfg.logFile}";
        printerConfigPath =
          if cfg.mutableConfig
          then cfg.mutableConfigFolder + "/printer.cfg"
          else "/etc/klipper.cfg";
        printerConfigFile =
          if cfg.settings != null
          then format.generate "klipper.cfg" cfg.settings
          else cfg.configFile;
      in {
        description = "Klipper 3D Printer Firmware";
        wantedBy = ["multi-user.target"];
        after = ["network.target"];
        preStart = ''
          mkdir -p ${cfg.mutableConfigFolder}
          ${lib.optionalString cfg.mutableConfig ''
            [ -e ${printerConfigPath} ] || {
              cp ${printerConfigFile} ${printerConfigPath}
              chmod +w ${printerConfigPath}
            }
          ''}
          mkdir -p ${cfg.mutableConfigFolder}/gcodes
        '';

        serviceConfig =
          {
            ExecStart = "${cfg.package}/bin/klippy ${klippyArgs} ${printerConfigPath}";
            RuntimeDirectory = "klipper";
            StateDirectory = "klipper";
            SupplementaryGroups = ["dialout"];
            WorkingDirectory = "${cfg.package}/lib";
            OOMScoreAdjust = "-999";
            CPUSchedulingPolicy = "rr";
            CPUSchedulingPriority = 99;
            IOSchedulingClass = "realtime";
            IOSchedulingPriority = 0;
            UMask = "0002";
          }
          // (
            if cfg.user != null
            then {
              Group = cfg.group;
              User = cfg.user;
            }
            else {
              DynamicUser = true;
              User = "klipper";
            }
          );
      })
      cfg;

    # environment.systemPackages =
    # with pkgs;
    # let
    #   default = a: b: if a != null then a else b;
    #   firmwares = filterAttrs (n: v: v != null) (mapAttrs
    #     (mcu: { enable, enableKlipperFlash, configFile, serial }:
    #       if enable then
    #         pkgs.klipper-firmware.override
    #           {
    #             mcu = lib.strings.sanitizeDerivationName mcu;
    #             firmwareConfig = configFile;
    #           } else null)
    #     cfg.firmwares);
    #   firmwareFlasher = mapAttrsToList
    #     (mcu: firmware: pkgs.klipper-flash.override {
    #       mcu = lib.strings.sanitizeDerivationName mcu;
    #       klipper-firmware = firmware;
    #       flashDevice = default cfg.firmwares."${mcu}".serial cfg.settings."${mcu}".serial;
    #       firmwareConfig = cfg.firmwares."${mcu}".configFile;
    #     })
    #     (filterAttrs (mcu: firmware: cfg.firmwares."${mcu}".enableKlipperFlash) firmwares);
    # in
    # [ klipper-genconf ] ++ firmwareFlasher ++ attrValues firmwares;
  };
}
