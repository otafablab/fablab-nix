# source: https://github.com/NixOS/nixpkgs/blob/3d0b29c970a45bc8fea9c70c632882184e5bc161/nixos/modules/services/misc/klipper.nix
{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.niklash.services.klippers;
  formatINI = import ./formatINI.nix {inherit lib;};
in {
  ##### interface
  options = {
    niklash.services.klippers = mkOption {
      default = {};
      type = types.listOf (types.submodule ({config, ...}: {
        options = {
          name = mkOption {
            type = types.str;
            description =
              lib.mdDoc
              "Name of the klipper service. Used in service files as a base path by default.";
          };

          package = mkOption {
            type = types.package;
            default = pkgs.klipper;
            defaultText = literalExpression "pkgs.klipper";
            description = lib.mdDoc "The Klipper package.";
          };

          logFile = mkOption {
            type = types.nullOr types.path;
            default = null;
            example = "/var/lib/${config.name}/klipper.log";
            description = lib.mdDoc ''
              Path of the file Klipper should log to.
              If `null`, it logs to stdout, which is not recommended by upstream.
            '';
          };

          inputTTY = mkOption {
            type = types.path;
            default = "/run/${config.name}/tty";
            description =
              lib.mdDoc "Path of the virtual printer symlink to create.";
          };

          apiSocket = mkOption {
            type = types.nullOr types.path;
            default = "/run/${config.name}/api";
            description = lib.mdDoc "Path of the API socket to create.";
          };

          # Not yet supported
          # mutableConfig = mkOption {
          #   type = types.bool;
          #   default = false;
          #   example = true;
          #   description = lib.mdDoc ''
          #     Whether to copy the config to a mutable directory instead of using the one directly from the nix store.
          #     This will only copy the config if the file at `services.klipper.mutableConfigPath` doesn't exist.
          #   '';
          # };

          # mutableConfigFolder = mkOption {
          #   type = types.path;
          #   default = "/var/lib/${config.name}";
          #   description = lib.mdDoc "Path to mutable Klipper config file.";
          # };

          # TODO disable support for configFile as it can be generated using ini2nix.py
          configFile = mkOption {
            type = types.nullOr types.path;
            default = null;
            description = lib.mdDoc ''
              Path to Klipper config. (deprecated)
            '';
          };

          user = mkOption {
            type = types.nullOr types.str;
            default = null;
            description = lib.mdDoc ''
              User account under which Klipper runs.

              If null is specified (default), a temporary user will be created by systemd.
            '';
          };

          group = mkOption {
            type = types.nullOr types.str;
            default = null;
            description = lib.mdDoc ''
              Group account under which Klipper runs.

              If null is specified (default), a temporary user will be created by systemd.
            '';
          };

          settings = mkOption {
            type = types.nullOr (pkgs.formats.ini {}).type;
            default = null;
            description = lib.mdDoc ''
              Configuration for Klipper. See the [documentation](https://www.klipper3d.org/Overview.html#configuration-and-tuning-guides)
              for supported values.
            '';
          };

          firmwares = mkOption {
            description = lib.mdDoc "Firmwares klipper should manage";
            default = {};
            type = with types;
              attrsOf (submodule {
                options = {
                  enable = mkEnableOption (lib.mdDoc ''
                    building of firmware for manual flashing.
                  '');
                  enableKlipperFlash = mkEnableOption (lib.mdDoc ''
                    flashings scripts for firmware. This will add `klipper-flash-$mcu` scripts to your environment which can be called to flash the firmware.
                    Please check the configs at [klipper](https://github.com/Klipper3d/klipper/tree/master/config) whether your board supports flashing via `make flash`.
                  '');
                  serial = mkOption {
                    type = types.nullOr path;
                    description =
                      lib.mdDoc
                      "Path to serial port this printer is connected to. Leave `null` to derive it from `service.klipper.settings`.";
                  };
                  configFile = mkOption {
                    type = path;
                    description =
                      lib.mdDoc
                      "Path to firmware config which is generated using `klipper-genconf`";
                  };
                };
              });
          };
        };
      }));
    };
  };

  ##### implementation
  config = {
    nixpkgs.overlays = [(import ./klipper-overlay.nix)];

    # For some reason moving systemd.services inside the mapping causes infinite recursion
    systemd.services = builtins.listToAttrs (builtins.map (cfg: let
        klippyArgs =
          "--input-tty=${cfg.inputTTY}"
          + optionalString (cfg.apiSocket != null)
          " --api-server=${cfg.apiSocket}"
          + optionalString (cfg.logFile != null) " --logfile=${cfg.logFile}";
        printerConfigFile =
          if cfg.settings != null
          then builtins.toFile "klipper.cfg" (formatINI cfg.settings)
          else cfg.configFile;
        inherit (cfg) name; # name of the service
      in {
        inherit name;
        value = {
          description = "Klipper 3D Printer Firmware";
          wantedBy = ["multi-user.target"];
          after = ["network.target"];
          preStart = ''
            mkdir -p /var/lib/${name}/gcodes
          '';
          serviceConfig = {
            ExecStart = "${cfg.package}/bin/klippy ${klippyArgs} ${printerConfigFile}";
            RuntimeDirectory = name;
            StateDirectory = name;
            SupplementaryGroups = ["dialout"];
            WorkingDirectory = "${cfg.package}/lib";
            OOMScoreAdjust = "-999";
            CPUSchedulingPolicy = "rr";
            CPUSchedulingPriority = 99;
            IOSchedulingClass = "realtime";
            IOSchedulingPriority = 0;
            UMask = "0002";
            Group = cfg.group;
            User = cfg.user;
          };
        };
      })
      cfg);

    ## TODO support firmware flasher
    # environment.systemPackages =
    # with pkgs;
    # let
    #   default = a: b: if a != null then a else b;
    #   firmwares = filterAttrs (n: v: v != null) (mapAttrs
    #     (mcu: { enable, enableKlipperFlash, configFile, serial }:
    #       if enable then
    #         pkgs.klipper-firmware.override
    #           {
    #             mcu = lib.strings.sanitizeDerivationName mcu;
    #             firmwareConfig = configFile;
    #           } else null)
    #     cfg.firmwares);
    #   firmwareFlasher = mapAttrsToList
    #     (mcu: firmware: pkgs.klipper-flash.override {
    #       mcu = lib.strings.sanitizeDerivationName mcu;
    #       klipper-firmware = firmware;
    #       flashDevice = default cfg.firmwares."${mcu}".serial cfg.settings."${mcu}".serial;
    #       firmwareConfig = cfg.firmwares."${mcu}".configFile;
    #     })
    #     (filterAttrs (mcu: firmware: cfg.firmwares."${mcu}".enableKlipperFlash) firmwares);
    # in
    # [ klipper-genconf ] ++ firmwareFlasher ++ attrValues firmwares;
  };
}
