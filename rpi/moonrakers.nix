{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.niklash.services.moonrakers;

  format = pkgs.formats.ini {
    # https://github.com/NixOS/nixpkgs/pull/121613#issuecomment-885241996
    listToValue = l:
      if builtins.length l == 1
      then generators.mkValueStringDefault {} (head l)
      else lib.concatMapStrings (s: "\n  ${generators.mkValueStringDefault {} s}") l;
    mkKeyValue = generators.mkKeyValueDefault {} ":";
  };
in {
  options = {
    niklash.services.moonrakers = mkOption {
      default = {};
      type = types.listOf (
        types.submodule (_: {
          options = {
            name = mkOption {
              type = types.str;
              description =
                lib.mdDoc
                "Name of the moonraker service. Used in service files as a base path by default.";
            };

            package = mkOption {
              type = types.package;
              default = pkgs.moonraker;
              defaultText = literalExpression "pkgs.moonraker";
              description = lib.mdDoc "The Moonraker package.";
            };

            klipperSocket = mkOption {
              type = types.path;
              # default = config.services.klipper.apiSocket;
              # defaultText = literalExpression "config.services.klipper.apiSocket";
              description = lib.mdDoc "Path to Klipper's API socket.";
            };

            user = mkOption {
              type = types.str;
              default = "moonraker";
              description = lib.mdDoc "User account under which Moonraker runs.";
            };

            group = mkOption {
              type = types.str;
              default = "moonraker";
              description = lib.mdDoc "Group account under which Moonraker runs.";
            };

            settings = mkOption {
              # type = (pkgs.formats.ini {}).type;
              inherit (format) type;
              default = {};
              example = {
                authorization = {
                  trusted_clients = ["10.0.0.0/24"];
                  cors_domains = ["https://app.fluidd.xyz" "https://my.mainsail.xyz"];
                };
              };
              description = lib.mdDoc ''
                Configuration for Moonraker. See the [documentation](https://moonraker.readthedocs.io/en/latest/configuration/)
                for supported values.
              '';
            };

            ## Unsupported for now, as it requires changes to policykit
            #   allowSystemControl = mkOption {
            #     type = types.bool;
            #     default = false;
            #     description = lib.mdDoc ''
            #   Whether to allow Moonraker to perform system-level operations.

            #   Moonraker exposes APIs to perform system-level operations, such as
            #   reboot, shutdown, and management of systemd units. See the
            #   [documentation](https://moonraker.readthedocs.io/en/latest/web_api/#machine-commands)
            #   for details on what clients are able to do.
            # '';
            #   };
          };
        })
      );
    };
  };

  # environment.etc."moonraker.cfg".source = let
  #   forcedConfig = {
  #     server = {
  #       host = cfg.address;
  #       port = cfg.port;
  #       klippy_uds_address = cfg.klipperSocket;
  #     };
  #     machine = {
  #       validate_service = false;
  #     };
  #   } // (lib.optionalAttrs (cfg.configDir != null) {
  #     file_manager = {
  #       config_path = cfg.configDir;
  #     };
  #   });
  #   fullConfig = recursiveUpdate cfg.settings forcedConfig;
  # in format.generate "moonraker.cfg" fullConfig;

  # systemd.tmpfiles.rules = [
  #   "d '${cfg.stateDir}' - ${cfg.user} ${cfg.group} - -"
  # ] ++ lib.optional (cfg.configDir != null) "d '${cfg.configDir}' - ${cfg.user} ${cfg.group} - -";

  config = {
    systemd.services = builtins.listToAttrs (builtins.map (cfg: let
        # TODO get linked klipper
        inherit (cfg) name;
        stateDir = "/var/lib/${name}";
        fullSettings =
          lib.recursiveUpdate {
            server.klippy_uds_address = cfg.klipperSocket;
            machine.validate_service = false;
          }
          cfg.settings;
        # configFile = builtins.toFile "moonraker.cfg" (formatINI fullSettings);
        configFile = format.generate "moonraker.cfg" fullSettings;
      in {
        inherit name;
        value = {
          description = "Moonraker, an API web server for Klipper";
          wantedBy = ["multi-user.target"];
          after = ["network.target"];
          # TODO ++ optional config.services.klipper.enable "klipper.service";

          # Create state directory and logs to avoid unwritable log file error
          preStart = ''
            mkdir -p ${stateDir}/logs
          '';

          # Needs `ip` command
          path = [pkgs.iproute2];

          serviceConfig = {
            ExecStart = "${cfg.package}/bin/moonraker -d ${stateDir} -c ${configFile}";
            StateDirectory = name;
            WorkingDirectory = stateDir; # same as StateDirectory
            PrivateTmp = true;
            OOMScoreAdjust = "-999";
            CPUSchedulingPolicy = "rr";
            CPUSchedulingPriority = 99;
            IOSchedulingClass = "realtime";
            IOSchedulingPriority = 0;
            UMask = "0002";
            Group = cfg.group;
            User = cfg.user;
          };
        };
      })
      cfg);
  };
}
