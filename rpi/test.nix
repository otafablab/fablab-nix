# Run with `nix eval --impure --expr 'import ./test.nix'`
with (import <nixpkgs> {}); let
  addIndent = {
    indentation ? "\t",
    stripPrefix ? true,
  }: str:
    (
      if stripPrefix
      then lib.strings.removePrefix indentation
      else id
    ) (lib.strings.concatMapStringsSep "\n" (line: "${indentation}${line}") (lib.strings.splitString "\n" str));
in
  builtins.trace (lib.generators.toINI {mkKeyValue = lib.generators.mkKeyValueDefault {mkValueString = addIndent {indentation = "  ";};} ":";} (import ./ultimaker-2-plus.nix)) ""
