{
  config,
  lib,
  pkgs,
  ...
}:
let
  publicDnsServer = "1.1.1.1";
  lanCIDR = "10.42.0.0/24"; # hardcoded elsewhere, use ripgrep to account
  routerAddress = "10.42.0.1";
  lanDHCPRange = "10.42.0.64 - 10.42.0.254";
  dmzRouterAddress = "10.42.0.2";
in
{
  # TODO check https://github.com/seandheath/nixos/blob/main/hosts/router.nix
  imports = [
    ./hardware-configuration.nix
    ../modules/autoUpdate.nix
    ../modules/core.nix
    ../modules/admins.nix
    ../modules/pxe
  ];

  boot = {
    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = true;
    kernelPackages = pkgs.linuxPackages_latest;
    kernel.sysctl = {
      "net.ipv4.conf.all.forwarding" = true;
    };
  };

  networking = {
    hostName = "rtr";

    nameservers = [ "${publicDnsServer}" ];
    firewall.enable = false;

    interfaces = {
      enp0s25 = {
        useDHCP = false;
        ipv4.addresses = [
          {
            address = routerAddress;
            prefixLength = 24;
          }
        ];
      };
    };
    defaultGateway = {
      address = dmzRouterAddress;
      interface = "enp0s25";
    };

    nftables = {
      enable = true;
      # Some example: https://wiki.nftables.org/wiki-nftables/index.php/Simple_ruleset_for_a_server
      ruleset = ''
        define LAN_IFACE = enp0s25
        define WG_IFACE = "ext"

        table ip filter {
          chain input {
            type filter hook input priority 0; policy drop;

            iifname lo accept comment "Allow traffic from localhost"
            iifname $LAN_IFACE accept comment "Allow local network to access the router"
            iifname $WG_IFACE accept comment "Allow WireGuard access"
            # iifname $WAN_IFACE tcp dport ssh accept comment "Allow SSH from WAN"
            # iifname $WAN_IFACE ct state established,related accept comment "Allow established traffic"
            # iifname $WAN_IFACE icmp type { echo-request, destination-unreachable, time-exceeded } counter accept comment "Allow select ICMP"
            # iifname $WAN_IFACE counter drop comment "Drop all other unsolicited traffic from wan"
            log prefix "[nftables] Inbound denied: " counter drop
          }

          chain forward {
            type filter hook forward priority filter; policy drop;
            # iifname $LAN_IFACE oifname $WAN_IFACE accept comment "Allow trusted LAN to WAN"
            iifname $LAN_IFACE oifname $WG_IFACE accept comment "Allow trusted LAN to WG"
            # iifname $WAN_IFACE oifname $LAN_IFACE ct state established, related accept comment "Allow established back to LANs"

            iifname $WG_IFACE oifname $LAN_IFACE accept comment "Allow connection to LAN from WireGuard"
            log prefix "[nftables] Forward denied: " counter drop
          }
        }

        table ip nat {
          chain postrouting {
            type nat hook postrouting priority 100; policy accept;

            # oifname $WAN_IFACE masquerade
            oifname $LAN_IFACE masquerade
          }

          chain prerouting {
            type nat hook prerouting priority dstnat; policy accept;
            iifname $LAN_IFACE ip daddr 65.109.234.4 dnat to 10.126.0.1 comment "Rewrite traffic to bastion through WireGuard"
          }
        }

        table ip6 filter {
          chain input {
            type filter hook input priority 0; policy drop;
          }
          chain forward {
            type filter hook forward priority 0; policy drop;
          }
        }
      '';
    };

    wireguard.interfaces.ext = {
      ips = [ "10.126.0.2/24" ];
      privateKeyFile = config.sops.secrets."wg/privatekey".path;
      peers = [
        {
          publicKey = "iCPI4WltI1bImzwBinnCaTglL5FEpwuuR+7WqHdwsRM=";
          allowedIPs = [ "10.126.0.1/32" ];
          endpoint = "65.109.234.4:53"; # NOTE if 53 gets blocked, use 123, or 443
          # Send keepalives every 25 seconds. Important to keep NAT tables alive.
          persistentKeepalive = 25;
        }
      ];
    };
  };

  services = {
    openssh.enable = true;
    openssh.settings = {
      PermitRootLogin = "yes";
      PasswordAuthentication = false;
    };

    kea.dhcp4 = {
      enable = true;
      settings = {
        interfaces-config = {
          interfaces = [ "enp0s25" ];
        };
        lease-database = {
          name = "/var/lib/kea/dhcp4.leases";
          persist = true;
          type = "memfile";
        };
        valid-lifetime = 4000;
        renew-timer = 1000;
        rebind-timer = 2000;
        expired-leases-processing = {
          reclaim-timer-wait-time = 10;
          flush-reclaimed-timer-wait-time = 25;
          hold-reclaimed-time = 3600;
          max-reclaim-leases = 100;
          max-reclaim-time = 250;
          unwarned-reclaim-cycles = 5;
        };
        # FIXME [TFTP] Send of "c8:1f:66:c1:7d:81/0" to 10.42.0.25:2072 failed: "10.42.0.25:2072": sending OACK: timeout waiting for ACK
        # next-server = "10.42.0.26";
        # boot-file-name = "/dev/null";
        subnet4 = [
          {
            pools = [ { pool = lanDHCPRange; } ];
            # TODO investigate ddns-qualifying-suffix
            subnet = lanCIDR;
            id = 1;
            option-data = [
              {
                name = "routers";
                # data = routerAddress;
                data = dmzRouterAddress;
              }
              {
                name = "domain-name-servers";
                data = publicDnsServer;
              }
            ];

            # TODO assign boot-file-name to select PXE image
            # https://kea.readthedocs.io/en/kea-1.8.0/arm/dhcp4-srv.html#next-server-siaddr
            reservations = [
              {
                hw-address = "90:1B:0E:46:95:6E";
                ip-address = "10.42.0.11";
                hostname = "amdahl";
              }
              {
                hw-address = "EC:B1:D7:4D:FA:FB";
                ip-address = "10.42.0.12";
                hostname = "babbage";
              }
              {
                hw-address = "EC:B1:D7:61:0F:A1";
                ip-address = "10.42.0.13";
                hostname = "carmack";
              }
              {
                hw-address = "3C:52:82:57:E6:87";
                ip-address = "10.42.0.14";
                hostname = "haskell-server";
                # hostname = "diffie";
              }
              {
                hw-address = "3C:52:82:5E:4B:1F";
                ip-address = "10.42.0.15";
                hostname = "edelman";
              }
              {
                hw-address = "40:B0:34:43:10:D4";
                ip-address = "10.42.0.16";
                hostname = "fredkin";
              }
              {
                hw-address = "3C:52:82:5E:4D:05";
                ip-address = "10.42.0.17";
                hostname = "gabriel";
              }
              {
                hw-address = "40:B0:34:43:E5:A9";
                ip-address = "10.42.0.18";
                hostname = "hellman";
              }
              {
                hw-address = "3C:52:82:5E:4B:89";
                ip-address = "10.42.0.19";
                hostname = "iverson";
              }
              {
                # Dell PowerEdge R420
                hw-address = "c8:1f:66:c1:7d:81";
                ip-address = "10.42.0.25";
                hostname = "ousterhout";
              }
              {
                # Dell PowerEdge R410
                hw-address = "00:26:b9:5a:45:03";
                ip-address = "10.42.0.26";
                hostname = "peyton-jones";
              }
              {
                # Cisco UCS220M3S
                hw-address = "88:5a:92:f6:e1:58";
                ip-address = "10.42.0.27";
                hostname = "reynolds";
              }
              # {
              #   hw-address = "88:5A:92:F6:E1:54";
              #   hostname = "reynolds-ipmi";
              # }
              {
                # Supero X8DTH
                hw-address = "00:25:90:35:0b:bc";
                ip-address = "10.42.0.28";
                hostname = "shamir";
              }
              # ipmi?
              # {
              #   hw-address = "c8:f7:50:64:5c:ef";
              # }
              {
                # Supero X7SBU
                hw-address = "00:30:48:d9:6f:18";
                ip-address = "10.42.0.29";
                hostname = "thompson";
              }
              {
                hw-address = "B8:27:EB:26:6C:96";
                ip-address = "10.42.0.48";
                hostname = "fablab-raspberrypi-1";
              }
            ];
          }
        ];
      };
    };

    nginx = {
      enable = true;
      recommendedProxySettings = true;
      virtualHosts = {
        "binarycache.10.42.0.1.nip.io" = lib.mkIf config.services.nix-serve.enable {
          locations."/".proxyPass = "http://${config.services.nix-serve.bindAddress}:${toString config.services.nix-serve.port}";
        };
      };
    };
    nix-serve = {
      enable = true;
      secretKeyFile = config.sops.secrets."nix-serve/priv-key".path;
    };
  };

  sops = {
    defaultSopsFile = ../secrets/rtr.yaml;
    secrets."nix-serve/priv-key" = { };
    secrets."wg/privatekey" = { };
  };

  # https://search.nixos.org/options?channel=21.11&show=system.stateVersion&from=0&size=50&sort=relevance&type=packages&query=stateVersion
  system.stateVersion = "21.05";
}
