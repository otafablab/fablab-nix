{pkgs}: let
  nvidiaDriver = pkgs.linuxPackages.nvidia_x11;
in
  (pkgs.buildFHSUserEnv {
    name = "cuda-env";
    targetPkgs = pkgs:
      with pkgs; [
        nvidiaDriver
        git
        gitRepo
        gnupg
        autoconf
        curl
        procps
        gnumake
        util-linux
        m4
        gperf
        unzip
        cudatoolkit
        libGLU
        libGL
        xorg.libXi
        xorg.libXmu
        freeglut
        xorg.libXext
        xorg.libX11
        xorg.libXv
        xorg.libXrandr
        zlib
        ncurses5
        stdenv.cc
        binutils
      ];
    multiPkgs = pkgs: with pkgs; [zlib];
    runScript = "bash";
    profile = ''
      export CUDA_PATH=${pkgs.cudatoolkit}
      export LD_LIBRARY_PATH=${nvidiaDriver}/lib
      export EXTRA_LDFLAGS="-L/lib -L${nvidiaDriver}/lib"
      export EXTRA_CCFLAGS="-I/usr/include"
    '';
  })
  .env
