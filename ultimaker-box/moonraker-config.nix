# https://moonraker.readthedocs.io/en/latest/configuration/
{
  file_manager.enable_object_processing = false;
  authorization = {
    cors_domains = [ "*" ];
    trusted_clients = [ "0.0.0.0/0" ];
  };
  # TODO Warning:
  # Unparsed config section [machines] detected. This may be the result of
  # a component that failed to load. In the future this will result in a startup
  # error.
  # machines.provider = "none"; # If the provider is set to "none" service action APIs will be disabled

  # machine.validate_service = false; # TODO does this work?
  server = {
    # Set by nix module
    # host = "0.0.0.0";
    # port = 7125;

    # Not needed when proxying based on subdomains
    # route_prefix = "";
    max_websocket_connections = 500; # Default is 50
  };
  history = { };
  # TODO spoolman
}
