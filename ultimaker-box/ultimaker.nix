{ config, lib, ... }:
let
  formatINI = import ../kube/cluster/pronter-ctrl/formatINI.nix { inherit lib; };
in
{
  services.klipper = {
    enable = true;
    configFile = builtins.toFile "klipper.cfg" (
      formatINI (
        lib.recursiveUpdate {
          mcu.serial = "/dev/serial/by-path/pci-0000:00:1a.0-usb-0:1.4:1.0";
          virtual_sdcard.path = "/var/lib/moonraker/gcodes";
        } (import ../kube/cluster/pronter-ctrl/ultimaker-2-plus.nix)
      )
    );
    inherit (config.services.moonraker) user group;
  };

  services.moonraker = {
    enable = true;
    allowSystemControl = true;
    port = 7125;
    settings = (import ./moonraker-config.nix);
  };

  services.mainsail = {
    enable = true;
    nginx.extraConfig = ''
      client_max_body_size 1000m;
    '';
  };
}
